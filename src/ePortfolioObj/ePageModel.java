package ePortfolioObj;
import ePage.Components.Component;
import ePortfolioGenerator.EPortfolioGenerator;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

public class ePageModel extends Tab
{       
        String Name;
        String myStyle;
        String myLayout;
        JsonObject data;
        

        ScrollPane SelectionsComp;
        VBox ContentVbox = new VBox();

            
    public String getName() {
        return Name;
    }
//        Tab theTab;
   private ArrayList<Component> allComps ;
        

    public ePageModel(ePortfolio Master) {
        //this.theTab = new Tab();
        Name = new String();
        myStyle = "Style 1";
        myLayout = "Layout 1";
        this.SelectionsComp = new ScrollPane(ContentVbox);
        this.setContent(SelectionsComp);
        this.allComps = new ArrayList<Component>();
        ContentVbox.setSpacing(20);
        Master.getAllPages().add(this);
    }
      public ePageModel(ePortfolio Master , String Style, String Layout) {
        //this.theTab = new Tab();
        Name = new String();
        myStyle = Style;
        myLayout = Layout;
        this.SelectionsComp = new ScrollPane(ContentVbox);
        this.setContent(SelectionsComp);
        this.allComps = new ArrayList<Component>();
        ContentVbox.setSpacing(20);
        Master.getAllPages().add(this);
    }
    public void setName(String Name)
    {
        this.Name = Name; 
        this.setText(Name);
    
    }
//    public Tab getTab()
//    {
//    return this.theTab;
//    }
    public void addToArrayList(Component addComp)
    {
        getAllComps().add(addComp);
    
    }
    public void removeFromArrayList(Component removeComp)
    {
        
        getAllComps().remove(removeComp);
    
    }
    public boolean containsComp(Tab find)
    {
        
    return getAllComps().contains(find);
    
    }
    public ArrayList<Component> getComponentArrayList()
    {
        return this.getAllComps();
    
    
    }

    /**
     * @return the allComps
     */
    public ArrayList<Component> getAllComps() {
        return allComps;
    }
    public void getAllData(){
        
           for(Component i:this.allComps)
           {  
               i.getInformation();
                        }

     
           }
    
    /*public void saveforJson()
       {
                  String jsonFilePath = "testjson/first.json";
                  

           
           JsonArrayBuilder componentsBuilder = Json.createArrayBuilder();
           
           
           for(Component component : allComps){
               componentsBuilder.add(component.saveToJson());
           }
           
           JsonArray components = componentsBuilder.build();
           
           JsonObject ePage = Json.createObjectBuilder()
                   .add("name", this.getName())
                   .add("components", components)
                   .build();
           
           
            try {
                 
                Map<String, Object> properties = new HashMap<>(1);
                 properties.put(JsonGenerator.PRETTY_PRINTING, true);
                 
                 OutputStream os = new FileOutputStream(jsonFilePath);
                 
                JsonWriterFactory jf = Json.createWriterFactory(properties);
                JsonWriter jWriter = jf.createWriter(os);
                 
                 jWriter.writeObject(ePage);
                 jWriter.close();
                 os.close();
                 
             } 
            catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
           
       }*/
     public void saveToJson()
     {
            String jsonFilePath = "testjson/"+this.getName()+".json";
                  

           
           JsonArrayBuilder componentsBuilder = Json.createArrayBuilder();
           
           
           for(Component component : allComps){
               componentsBuilder.add(component.saveToJson());
           }
           
           JsonArray components = componentsBuilder.build();
           
           JsonObject ePage = Json.createObjectBuilder()
                   .add("name", this.getName())
                   .add("style", this.myStyle)
                   .add("layout",this.myLayout)
                   .add("components", components)
                   
                   .build();
       
           
            try {
                 
                Map<String, Object> properties = new HashMap<>(1);
                 properties.put(JsonGenerator.PRETTY_PRINTING, true);
                 
                 OutputStream os = new FileOutputStream(jsonFilePath);
                 
                JsonWriterFactory jf = Json.createWriterFactory(properties);
                JsonWriter jWriter = jf.createWriter(os);
                 
                 jWriter.writeObject(ePage);
                 jWriter.close();
                 os.close();
                 
             } catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
     
     
     }
     
      public JsonObject toJson()
     {
                  

           
           JsonArrayBuilder componentsBuilder = Json.createArrayBuilder();
           
           
           for(Component component : allComps){
               componentsBuilder.add(component.saveToJson());
           }
           
           JsonArray components = componentsBuilder.build();
           
           JsonObject ePage = Json.createObjectBuilder()
                   .add("page name", this.getName())
                   .add("style", this.myStyle)
                   .add("layout",this.myLayout)
                   .add("components", components)
                   .build();
       
           
        
             return ePage;
     
     
     }
    

    public String getMyStyle() {
        return myStyle;
    }

    public void setMyStyle(String myStyle) {
        this.myStyle = myStyle;
    }

    public String getMyLayout() {
        return myLayout;
    }

    public void setMyLayout(String myLayout) {
        this.myLayout = myLayout;
    }
  
}
        
      
        
            
       
        

          




