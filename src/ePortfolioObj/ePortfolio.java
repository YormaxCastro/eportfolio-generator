/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioObj;

import EPortfolio.View.ActionDialogsBox;
import EPortfolio.View.ActionHbox;
import ePage.Components.BannerComponent;
import ePage.Components.BannerImage;
import ePage.Components.Component;
import ePage.Components.FooterComponent;
import ePage.Components.HeaderComponent;
import ePage.Components.ImageComponent;
import ePage.Components.ListComponent;
import ePage.Components.Slide;
import ePage.Components.SlideShowComponent;
import ePage.Components.TextComponent;
import ePage.Components.VideoComponent;
import ePortfolioGenerator.EPortfolioGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author ycastro
 */
public class ePortfolio extends TabPane {

    private String title;
    private String studentName;
    private JsonObject portfolioData;
    private String absPath; 

    private ArrayList<ePageModel> allPages = new ArrayList<ePageModel>();
    ePageModel CurrentPage;

    public ePortfolio(String title, String studentName, ArrayList<ePageModel> allPages) {
        this.title = title;
        this.studentName = studentName;
        this.allPages = allPages;

    }
    
      public void LoadJson(File loadFile)
    {
         try {
                InputStream is = new FileInputStream(loadFile);
                JsonReader jReader = Json.createReader(is);
                JsonObject ob = jReader.readObject();
                  
                //System.out.printf("Title: %s Student Name: %s Page Names : \n", ob.getString("title"), ob.getString("student name"));
                     
                 
                 JsonArray pagesArray = ob.getJsonArray("pages");
                 
                 
                        for(int j = 0; j < pagesArray.size(); j++)
                      {
                          
                           // makes the page and the tab on the gui 
                     ePageModel tempPage = new ePageModel(this);
                     this.getTabs().add(tempPage);
                     
                     JsonObject page = pagesArray.getJsonObject(j);
                    String pageName = page.getString("page name");
                    
                    String Style = page.getString("style");
                    String Layout = page.getString("layout");


                    tempPage.setText(pageName);
                    tempPage.setMyStyle(Style);
                    tempPage.setMyLayout(Layout);
                    //
                      JsonArray pageComps = page.getJsonArray("components");
                          sortAndSetCompList(pageComps,tempPage);
                           
                          
                           
                          // #
                           
                   

                  
                           
                           
                      }
                        
                        
                        
//                    JsonArray compsOfPage = ob.getJsonArray("components");
//                 
//                 
//                 
//                 
//                 
//                      for(int j = 0; j < ob2.size(); j++)
//                      {
//                           JsonObject obPage = ob2.getJsonObject(j);
//                           String tempPath = "./testjson/"+obPage.getString("name")+".json";
//                           System.out.print(tempPath);
//                           File temp = new File(tempPath);
//                           //(temp);
//
//                     //System.out.printf("name: %s \n", obPage.getString("name"));
//                   
//                     ePageModel loadPage = new ePageModel(this);
//                     this.getTabs().add(loadPage);
//                   
//                     
//                     loadPage.setText(obPage.getString("name"));
//                     
//                      }
                
                 
                 
             }
             catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
    
    }
public void sortAndSetCompList(JsonArray pageComps, ePageModel page)

{
    for(int j = 0; j < pageComps.size(); j++) 
    {    
        String typeOfComp = pageComps.getJsonObject(j).getString("type");
        
        switch (typeOfComp)
        {
            
            case "text component": 
                
                TextComponent temp = new TextComponent(pageComps.getJsonObject(j).getString("font") ,
                        pageComps.getJsonObject(j).getString("fsize") ,
                        pageComps.getJsonObject(j).getString("content")); 
                
                
            ScrollPane theScroll =  (ScrollPane)page.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            MasterContent.getChildren().add(temp);
                break;
            
            case "Footer":
                
                FooterComponent temp1 = new FooterComponent(
               pageComps.getJsonObject(j).getString("title")

                );
                
                
            ScrollPane theScroll1 =  (ScrollPane)page.getContent();
            VBox MasterContent1 = (VBox)theScroll1.getContent();
            MasterContent1.getChildren().add(temp1);
                break;
                
           case"banner":
             BannerComponent temp3 = new BannerComponent();
             String title = pageComps.getJsonObject(j).getString("title");
             boolean incIm = pageComps.getJsonObject(j).getBoolean("includeimage");
           
            
             

             
             temp3.setTitle(title);
             temp3.getStgInput().setText(title);
             temp3.setIncIm(incIm);
               if(incIm)
               {
             String imagePath = pageComps.getJsonObject(j).getString("imagepath");
             String height = pageComps.getJsonObject(j).getString("height");
             String width = pageComps.getJsonObject(j).getString("width");
             System.out.println("height: " + height);
            BannerImage BannerImg = new BannerImage();
                   temp3.getButton2().setSelected(incIm);
                   BannerImg.getWidthInput().setText(height);
                   BannerImg.setHeightInput(new TextField(width));
                   BannerImg.setFilePath(imagePath);
                   BannerImg.setIv1(new ImageView(new Image("file:"+imagePath)));
                   System.out.print(imagePath);
            temp3.getLayout().setCenter(BannerImg);
            incIm = true;
                   
            
               
               }
            ScrollPane theScroll2 =  (ScrollPane)page.getContent();
            VBox MasterContent2 = (VBox)theScroll2.getContent();
            MasterContent2.getChildren().add(temp3);
               break;
            case "video": 
                
                VideoComponent temp4 = new VideoComponent(
                        pageComps.getJsonObject(j).getString("title") ,
                        pageComps.getJsonObject(j).getString("filepath") ,
                        pageComps.getJsonObject(j).getString("caption"),
                        pageComps.getJsonObject(j).getString("width"),
                        pageComps.getJsonObject(j).getString("height")
                ); 
                
                
            ScrollPane theScroll4 =  (ScrollPane)page.getContent();
            VBox MasterContent4 = (VBox)theScroll4.getContent();
            MasterContent4.getChildren().add(temp4);
                break;
                
          case "image": 
                
                ImageComponent temp5 = new ImageComponent(
                        pageComps.getJsonObject(j).getString("title") ,
                        pageComps.getJsonObject(j).getString("caption"),
                        pageComps.getJsonObject(j).getString("height"),
                        pageComps.getJsonObject(j).getString("width"),
                        pageComps.getJsonObject(j).getString("filepath") 
                ); 
                
                
            ScrollPane theScroll5 =  (ScrollPane)page.getContent();
            VBox MasterContent5 = (VBox)theScroll5.getContent();
            MasterContent5.getChildren().add(temp5);
                break;
            case "header": 
                
                HeaderComponent temp6 = new HeaderComponent(pageComps.getJsonObject(j).getString("title"));                
                
            ScrollPane theScroll6 =  (ScrollPane)page.getContent();
            VBox MasterContent6 = (VBox)theScroll6.getContent();
            MasterContent6.getChildren().add(temp6);
                break;
                
                
            case "list":
          ArrayList <String> list = new ArrayList<String>(); 
                JsonArray listItems = pageComps.getJsonObject(j).getJsonArray("items");
          for (int i = 0 ; i < listItems.size(); i++)
          {
                       list.add( listItems.getJsonObject(i).getString("listitem"));

          
          }
            
         ListComponent temp7 = new ListComponent(pageComps.getJsonObject(j).getString("title"),list,pageComps.getJsonObject(j).getString("font"));
       
         
         
                
            ScrollPane theScroll7 =  (ScrollPane)page.getContent();
            VBox MasterContent7 = (VBox)theScroll7.getContent();
            MasterContent7.getChildren().add(temp7);
                
                break;
               
            case "slide show":
                
                ArrayList <String> slides = new ArrayList<String>(); 
                ArrayList <String> caption = new ArrayList<String>(); 
                JsonArray slideItems = pageComps.getJsonObject(j).getJsonArray("slides");
          for (int i = 0 ; i < slideItems.size(); i++)
          {
                       slides.add( slideItems.getJsonObject(i).getString("filepath"));
                       caption.add(slideItems.getJsonObject(i).getString("caption"));
                       
          
          }
          
            
         SlideShowComponent temp8 = new  SlideShowComponent(pageComps.getJsonObject(j).getString("title"),slides,caption,"500","500");
       
         
         
                
            ScrollPane theScroll8 =  (ScrollPane)page.getContent();
            VBox MasterContent8 = (VBox)theScroll8.getContent();
            MasterContent8.getChildren().add(temp8);
                
               
                
                break;
               
              
               
               
               
               
            
            
            
            
                
        

        
        
        
        }
    }
     



} 
      
      
      
  // public loadAndSetComp(){}
      
      
//   public void loadPage(File loadFile)
//   {
//            try {
//                InputStream is = new FileInputStream(loadFile);
//                JsonReader jReader = Json.createReader(is);
//                JsonObject ob = jReader.readObject();
//                  
//                System.out.printf("Page name: %s \n", ob.getString("name"));
//                     
//                 
//                 JsonArray ob2 = ob.getJsonArray("components");
//                      for(int j = 0; j < ob2.size(); j++)
//                      {
//                           JsonObject obComp = ob2.getJsonObject(j);
//
//                           System.out.printf("type: %s \n", obComp.getString("type"));
//                      }
//                
//                 
//                 
//             }
//             catch (IOException ex) {
//                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
//             }
//   
//       
//       
//       
//   
//   }

    public ePortfolio() {
        this.title = "Unititled";
        this.studentName = "No Name";
        

    }

    public void addPage(ePageModel addPage) {
        this.getAllPages().add(addPage);
    }

    public void removePage(ePageModel addPage) {
        this.getAllPages().remove(addPage);
    }

    public void setCurrentPage(ePageModel CurrentPage) {
        this.CurrentPage = CurrentPage;

    }

    public ePageModel CurrentPage() {
        return this.CurrentPage;
    }

    public JsonObject getPortfolioData() {
        return portfolioData;
    }
   

    
    
    

    public void setPortfolioData() {

        JsonArrayBuilder ja = Json.createArrayBuilder();
        for (ePageModel i : getAllPages())
                    ja.add(i.toJson());
        
                  
       

        JsonArray array = ja.build();

        portfolioData = Json.createObjectBuilder()
                .add("title", this.getTitle())
                .add("studentname", this.getStudentName())
                .add("pages", array)
                .build();
        
        String dataPath = "./data/";
        String filePath = dataPath + title + ".json";
        this.setAbsPath(filePath);
//       use to make an export  
//        File newFile = new File(oath);
//        
//        bewFile.mkdir();
        
        
        Map<String, Object> properties = new HashMap<>(1);
                 properties.put(JsonGenerator.PRETTY_PRINTING, true);
                 
                OutputStream os;
        try {
            os = new FileOutputStream(filePath);
            JsonWriterFactory jf = Json.createWriterFactory(properties);
            JsonWriter jWriter = jf.createWriter(os);
                 
            jWriter.writeObject(portfolioData);
            jWriter.close();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(ePortfolio.class.getName()).log(Level.SEVERE, null, ex);
        }
                 
           

    }
      public void setAsPortfolioData(String savePath) {

        JsonArrayBuilder ja = Json.createArrayBuilder();
        for (ePageModel i : getAllPages())
                    ja.add(i.toJson());
        
                  
       

        JsonArray array = ja.build();

        portfolioData = Json.createObjectBuilder()
                .add("title", this.getTitle())
                .add("student name", this.getStudentName())
                .add("pages", array)
                .build();
//        String dataPath = "./data/";
//        String filePath = dataPath + title + ".json";
//       use to make an export  
//        File newFile = new File(oath);
//        
//        bewFile.mkdir();
        
        
        Map<String, Object> properties = new HashMap<>(1);
                 properties.put(JsonGenerator.PRETTY_PRINTING, true);
                 
                OutputStream os;
        try {
            os = new FileOutputStream(savePath);
            JsonWriterFactory jf = Json.createWriterFactory(properties);
            JsonWriter jWriter = jf.createWriter(os);
                 
            jWriter.writeObject(portfolioData);
            jWriter.close();
            os.close();
        } catch (IOException ex) {
            Logger.getLogger(ePortfolio.class.getName()).log(Level.SEVERE, null, ex);
        }
                 
           

    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @return the allPages
     */
    public ArrayList<ePageModel> getAllPages() {
        return allPages;
    }

    /**
     * @param allPages the allPages to set
     */
    public void setAllPages(ArrayList<ePageModel> allPages) {
        this.allPages = allPages;
    }
    
    public void clear()
    {
    
    
    }

    /**
     * @return the absPath
     */
    public String getAbsPath() {
        return absPath;
    }

    /**
     * @param absPath the absPath to set
     */
    public void setAbsPath(String absPath) {
        this.absPath = absPath;
    }
    
  

}


