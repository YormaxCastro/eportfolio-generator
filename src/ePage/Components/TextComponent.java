/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class TextComponent extends Component {
    BorderPane layout = new BorderPane();
    VBox bottomLayout = new VBox();
    VBox buttonBox = new VBox();
    VBox textSel = new VBox();
    VBox textCon = new VBox();
    Label text = new Label("The Text Content: ");
    TextArea textArea = new TextArea();
    Label textFont = new Label("Text Font : ");
    Button set = new Button("Set");
    ComboBox<String> myTextSelection = new ComboBox<String>();
    Label fontSzPrompt = new Label("Font Size:");
    TextField sizeInput = new TextField("12");
    VBox fontBox = new VBox(fontSzPrompt,sizeInput);
    // Actual Content
    String font;
    String fontSize;
    String textContent ;
    
    public TextComponent(String font , String fontSize,String textContent)
    {
    
    this.setType("text component");
            myTextSelection.getItems().addAll("Font 1","Font 2","Font 3","Font 4","Font 5");
            myTextSelection.getSelectionModel().selectFirst();
           
            textSel.getChildren().addAll(textFont,myTextSelection);
            textCon.getChildren().addAll(text,textArea);
            buttonBox.getChildren().add(set);
            buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
            bottomLayout.getChildren().addAll(textSel,fontBox,buttonBox);
            layout.setCenter(textCon);
            layout.setTop(this.getRemoveBox());
            layout.setBottom(bottomLayout);
            this.getChildren().add(layout);
            
            set.setOnAction(e->
            {
                if (textArea.getText().isEmpty())
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("An Empty Text Field !");

                alert.showAndWait();
                
                }
                else{
                    this.font = myTextSelection.getSelectionModel().getSelectedItem();
                    this.textContent = textArea.getText();
                    this.fontSize = sizeInput.getText();
                }
            
    this.font = font;
    this.fontSize = fontSize;
    this.textContent = textContent;
    this.textArea.setText(textContent);
    this.sizeInput.setText(fontSize);
    this.myTextSelection.getSelectionModel().select(font);
            }
            
            );
            
    
    }
    public TextComponent()
    {
            this.myTextSelection.getSelectionModel().select(font);
            this.setType("text component");
            myTextSelection.getItems().addAll("Font 1","Font 2","Font 3","Font 4","Font 5");
            myTextSelection.getSelectionModel().selectFirst();
           this.font = "12";
            textSel.getChildren().addAll(textFont,myTextSelection);
            textCon.getChildren().addAll(text,textArea);
            buttonBox.getChildren().add(set);
            buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
            bottomLayout.getChildren().addAll(textSel,fontBox,buttonBox);
            layout.setCenter(textCon);
            layout.setTop(this.getRemoveBox());
            layout.setBottom(bottomLayout);
            this.getChildren().add(layout);
            
            set.setOnAction(e->
            {
                if (textArea.getText().isEmpty())
                {
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("An Empty Text Field !");

                alert.showAndWait();
                
                }
                else{
                    this.font = myTextSelection.getSelectionModel().getSelectedItem();
                    this.textContent = textArea.getText();
                    this.fontSize = sizeInput.getText();
                }
            
            }
            
            );
            

            
    
    
    }
    @Override
    public void getInformation()
    {
    System.out.println(this.font + "  "+ this.textContent);
    
    }
    
       public JsonObject saveToJson(){
                JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle() )
                   .add("font",this.font)
                    .add("fsize",this.fontSize)
                    .add("content",this.textContent)
                   .build();
           
           
           return jso;
       }
    
    
    
    
}
