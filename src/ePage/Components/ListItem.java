/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author ycastro
 */
//Makes a List Item in which will be added to the 
public class ListItem extends HBox {
    Label NameLabel;
    TextField NameField;
    Button Set;
    private Button RemoveFromList ;
    public ListItem()
    {
        MakeListItem();
    
    }
    public ListItem(String x)
    {
        MakeListItem();
        this.NameField.setText(x);
    
    }
    public void MakeListItem(){
       NameLabel = new Label("List Item : ");
       NameField = new TextField();

        setRemoveFromList(new Button("Remove"));
        getRemoveFromList().setPrefSize(100,10);
       Set = new Button("Set");
       Set.setPrefSize(100,10);

       this.getChildren().addAll(NameLabel,NameField, getRemoveFromList(),Set);
       this.setSpacing(5);
       
       Set.setOnAction(e->
       {
       this.setTextContent(NameField.getText());
       System.out.println(getTextContent());
       }
       );
}

    /**
     * @return the Content
     */
    public String getTextContent() {
        return NameField.getText();
    }

    /**
     * @param Content the Content to set
     */
    public void setTextContent(String Content) {
        this.NameField.setText(Content);
    }

    /**
     * @return the RemoveFromList
     */
    public Button getRemoveFromList() {
        return RemoveFromList;
    }

    /**
     * @param RemoveFromList the RemoveFromList to set
     */
    public void setRemoveFromList(Button RemoveFromList) {
        this.RemoveFromList = RemoveFromList;
    }
}
