/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import static ePortfolioGenerator.StartupConstants.SLIDESHOW_COMPONENT_IMG;
import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class SlideShowComponent extends Component {
    BorderPane Layout = new BorderPane();
    ScrollPane slideShowScrollShell = new ScrollPane();
    SlideShowScrollPane theSlideScroll = new SlideShowScrollPane();
    
     Label CaptionLabel = new Label ("Title : ");
     TextField CaptionInput = new TextField(" ");
     HBox CaptionBox = new HBox(CaptionLabel,CaptionInput);
   
    HBox HeightBox = new HBox();
    Label HeightLabel = new Label("Height : ");
    TextField HeightInput = new TextField("500");
    
    
    
    HBox WidthBox = new HBox();
    Label WidthLabel = new Label("Width : ");
    TextField WidthInput = new TextField("500");
    
    Button set = new Button("Set");
    
    VBox Specs = new VBox(WidthBox,HeightBox,CaptionBox);
    Image SlideShowThumb = new Image(SLIDESHOW_COMPONENT_IMG);
    ImageView iv1 = new ImageView(SlideShowThumb);
    
    HBox ButtonBox = new HBox();

    // Actual Content 
    String myWidth = new String("500");
    String myHeight = new String("500");
    
      public SlideShowComponent(String string, ArrayList<String> slides, ArrayList<String> caption, String width , String height) 
      {
          this.setType("slide show");
       set.setPrefWidth(100);
       ButtonBox.setSpacing(5);
       ButtonBox.setAlignment(Pos.BOTTOM_RIGHT);
       
       HeightBox.getChildren().addAll(HeightLabel,HeightInput);
       WidthBox.getChildren().addAll(WidthLabel,WidthInput);
       ButtonBox.getChildren().addAll(set);
       
       slideShowScrollShell.setMaxHeight(400);
       slideShowScrollShell.setContent(theSlideScroll);
       
       
       this.getChildren().add(Layout);
       set.setOnAction(e->
       {
          this.myWidth = WidthInput.getText();
          this.myHeight = HeightInput.getText();
          this.setTitle( CaptionInput.getText());
          getInformation();
           theSlideScroll.getInformation();
       }
       
       );
       
          
          for(int i = 0 ; i < slides.size();i++)
          {
              slides.get(i);
              caption.get(i);
              
              Slide newSlide = new Slide(this.theSlideScroll, slides.get(i),caption.get(i),i);
              
              
               this.theSlideScroll.sizeOfSlides++;

  
    
    this.theSlideScroll.allSlide.add(newSlide);
    this.theSlideScroll.allSlideVBox.getChildren().add(newSlide);
    
          }
    this.theSlideScroll.allSlide.remove(this.theSlideScroll.firstSlide);
    this.theSlideScroll.allSlideVBox.getChildren().remove(this.theSlideScroll.firstSlide);
        
          this.WidthInput.setText(width);
          this.HeightInput.setText(height);
          this.setTitle( string);
          this.CaptionInput.setText(string);
       HBox left = new HBox(iv1,this.theSlideScroll.getSlideToolBar());
       
       this.ButtonBox.setAlignment(Pos.TOP_RIGHT);
       Layout.setTop(this.ButtonBox);
       
       Layout.setLeft(left);
       Layout.setRight(Specs);
       Layout.setCenter(this.theSlideScroll);
       Layout.setBottom(ButtonBox);
        Layout.setTop(this.getRemoveBox());
       this.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
      
      
      }
   public SlideShowComponent()
   {
       this.setType("slide show");
       set.setPrefWidth(100);
       ButtonBox.setSpacing(5);
       ButtonBox.setAlignment(Pos.BOTTOM_RIGHT);
       
       HeightBox.getChildren().addAll(HeightLabel,HeightInput);
       WidthBox.getChildren().addAll(WidthLabel,WidthInput);
       ButtonBox.getChildren().addAll(set);
       
       slideShowScrollShell.setMaxHeight(400);
       slideShowScrollShell.setContent(theSlideScroll);
       
       
       HBox left = new HBox(iv1,theSlideScroll.getSlideToolBar());
       Layout.setLeft(left);
      this.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
       Layout.setTop(this.getRemoveBox());
       Layout.setRight(Specs);
       Layout.setCenter(slideShowScrollShell);
       Layout.setBottom(ButtonBox);
       this.getChildren().add(Layout);
       set.setOnAction(e->
       {
          this.myWidth = WidthInput.getText();
          this.myHeight = HeightInput.getText();
          this.setTitle( CaptionInput.getText());
          getInformation();
           theSlideScroll.getInformation();
       }
       
       );
  
   }

  
   public void getInformation()
   {
   System.out.println(this.getTitle()+ this.myHeight+ this.myWidth);
   }
   public JsonObject saveToJson(){
       
          JsonArrayBuilder ja = Json.createArrayBuilder();
          for (Slide i :theSlideScroll.getAllSlide())
          {
              JsonObject jso1 = Json.createObjectBuilder()
              .add("filepath",i.getFilePath())
              .add("caption", i.getCaption())
              .build();
              ja.add(jso1);
          }
           
           JsonArray array = ja.build();
           
           JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle())
                   .add("height", this.getHeight())
                   .add("width", this.getWidth())
                   .add("slides",array)
                   .build();
           
           return jso;
       }
       
       
       
   
   
   }
    
    
       
    
    
    
    

