/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import java.math.BigDecimal;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class BannerComponent extends Component {

    BorderPane layout = new BorderPane();
    VBox TopBox = new VBox();

    HBox BannerStg = new HBox();
    Label Banner = new Label("Banner : ");
    private TextField StgInput = new TextField("Banner Title");


    HBox ImgSelection = new HBox();
    Label incImage = new Label("Include Image ?");
    ToggleGroup group = new ToggleGroup();
    RadioButton button1 = new RadioButton("No");
    private RadioButton button2 = new RadioButton("Yes");
    private boolean incIm = false;
    private BannerImage BannerImg = null;
    VBox setBox = new VBox();
    Button Set = new Button("Set");



    public BannerComponent() {
        this.setType("banner");
        BannerStg.getChildren().addAll(Banner, StgInput);
        BannerImg = new BannerImage();
        button1.setToggleGroup(group);
        button1.setSelected(true);
        button2.setToggleGroup(group);
        ImgSelection.getChildren().addAll(incImage, button1, button2);
        TopBox.getChildren().addAll(BannerStg, ImgSelection);

        button1.setOnAction(e -> {

            layout.setCenter(new HBox());
            BannerImg = null;
            incIm = false;

        });
        button2.setOnAction(e -> {

            BannerImg = new BannerImage();
            layout.setCenter(BannerImg);
            incIm = true;

        });
        setBox.getChildren().add(Set);
        setBox.setAlignment(Pos.BOTTOM_RIGHT);
        layout.setTop(TopBox);
        layout.setBottom(setBox);
        layout.setRight(this.getRemoveBox());
        this.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
        Set.setOnAction(e -> {
            

           // this.getBannerInformation();
        });

        this.getChildren().add(layout);
    }

    public void getBannerInformation() {
        if (this.getBannerImg() != null) {
            getBannerImg().getBannerImageInformation();
            System.out.println(this.getStgInput().getText());
        } else {
            System.out.println(this.getStgInput().getText());
        }

    }

    public TextField getStgInput() {
        return StgInput;
    }
    public void getInformation() {

        if (this.getBannerImg() != null) {
            getBannerImg().getBannerImageInformation();
            System.out.println(this.getStgInput().getText());
        } else {
            System.out.println(this.getStgInput().getText());
        }

    }

    public JsonObject saveToJson() {
        JsonObject jso;
        if (this.getBannerImg() != null) {

            jso = Json.createObjectBuilder()
                    .add("type", this.getType())
                    .add("title", getStgInput().getText())
                    .add("includeimage", isIncIm())
                    .add("imagepath", getBannerImg().getFilePath())
                    .add("height", getBannerImg().getHeightInput().getText())
                    .add("width", getBannerImg().getWidthInput().getText())
                    .build();

        } else {

            jso = Json.createObjectBuilder()
                    .add("type", this.getType())
                    .add("title", getStgInput().getText())
                    .build();
        }

        return jso;
    }

    /**
     * @param StgInput the StgInput to set
     */
    public void setStgInput(TextField StgInput) {
        this.StgInput = StgInput;
    }

    /**
     * @return the incIm
     */
    public boolean isIncIm() {
        return incIm;
    }

    /**
     * @param incIm the incIm to set
     */
    public void setIncIm(boolean incIm) {
        this.incIm = incIm;
    }

    /**
     * @return the BannerImg
     */
    public BannerImage getBannerImg() {
        return BannerImg;
    }

    /**
     * @param BannerImg the BannerImg to set
     */
    public void setBannerImg(BannerImage BannerImg) {
        this.BannerImg = BannerImg;
    }

    /**
     * @return the button2
     */
    public RadioButton getButton2() {
        return button2;
    }

    /**
     * @param button2 the button2 to set
     */
    public void setButton2(RadioButton button2) {
        this.button2 = button2;
    }

    public BorderPane getLayout() {
        return layout;
    }

    public void setLayout(BorderPane layout) {
        this.layout = layout;
    }

}
