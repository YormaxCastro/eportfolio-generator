/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import java.math.BigDecimal;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class ListComponent extends Component {
  
    VBox ListOfItems;
      
      HBox ListName;
      HBox ButtonBox;
      ScrollPane ListObjects;
      Button addAListItem;
      Button RemoveFromList;
      Label NameLabel;
      TextField NameField;
      Button SetButton;
      BorderPane root;
      // Stores all the List content 
      ArrayList <ListItem> allListItems = new ArrayList<ListItem>();
      ListItem firstItem;
      //font 
        Label fontPrompt = new Label("Page Font : ");
         ComboBox<String> myTextSelection = new ComboBox<String>();
            VBox textSel = new VBox(fontPrompt,myTextSelection);
            String font; 

      

    
    public ListComponent()
    {
        makeList();
    
    }

    public ListComponent(String string , ArrayList<String>array, String font ) {

            makeList();
            this.NameField.setText(string);
            this.setFont(font);
            this.myTextSelection.getSelectionModel().select(font);
            
            if(!array.isEmpty())
            {
               for(String i : array )
               {
               ListItem z = new ListItem(i);
               this.ListOfItems.getChildren().add(z);
               allListItems.add(z);
               
               }
            
            }
        this.allListItems.remove(firstItem);
        this.ListOfItems.getChildren().remove(firstItem);

    
    
    
    
    
    }


  
    public void makeList()
    {
     // Set the title of the List
        this.setType("list");
        NameLabel = new Label("Title of the List :");
        NameField = new TextField();
        ListName = new HBox(NameLabel,NameField);
        
        
        
        
        //first List item is made
        firstItem = new ListItem();
        this.allListItems.add(firstItem);
       // ListContent.add(firstItem.getTextContent());
       
        //Vbox where all items are are added 
        ListOfItems = new VBox(firstItem);
        

        
      //Makes the Scroll for all List Objects
       ListObjects = new ScrollPane();
       ListObjects.setContent(ListOfItems);
       
       //the Add button for adding more List items
      
       addAListItem = new Button("Add an Item");
       addAListItem.setOnAction(e -> {
           ListItem Item = new ListItem();
           
           Item.getRemoveFromList().setOnAction(x ->
        {
            this.allListItems.remove(Item);
            ListOfItems.getChildren().remove(Item);
            
            
        
        });
          this.allListItems.add(Item);
          
          ListOfItems.getChildren().add(Item); 

       
       } );
       
       SetButton = new Button("Set List");
       SetButton.setOnAction(e->
       {
       this.setFont( myTextSelection.getSelectionModel().getSelectedItem());
       this.setTitle(NameField.getText());
       System.out.println(this.getTitle());
       
       this.getInformation();
       }
       );
       ButtonBox = new HBox();
       ButtonBox.getChildren().addAll(addAListItem,SetButton);
       ButtonBox.setAlignment(Pos.BOTTOM_RIGHT);
       VBox bottomBox = new VBox(textSel,ButtonBox);
        myTextSelection.getItems().addAll("Font 1","Font 2","Font 3","Font 4","Font 5");
        myTextSelection.getSelectionModel().selectFirst();
      
        this.setFont("Font 1");
       
       root = new BorderPane(); 
       root.setTop(ListName);
       root.setBottom(bottomBox);
       root.setCenter(ListObjects);
       root.setRight(this.getRemoveBox());
       this.RemoveBox.setAlignment(Pos.TOP_RIGHT);
       
       
       
        firstItem.getRemoveFromList().setOnAction(e ->
        {
            this.allListItems.remove(firstItem);
            ListOfItems.getChildren().remove(firstItem);
            
            
        
        }
        
        );
     
        
       this.getChildren().add(root);
       
      
    }
    //here you would extract the information give it to the json 
    public void getInformation()
    {
          for (int i = 0 ; i < this.allListItems.size();i++) {
              System.out.println(this.allListItems.get(i).getTextContent());
          }
    
    
    } 
      public JsonObject saveToJson(){
       
          JsonArrayBuilder ja = Json.createArrayBuilder();
          for (ListItem i :allListItems)
          {
              JsonObject jso1 = Json.createObjectBuilder()
              .add("listitem",i.getTextContent()).build();
              ja.add(jso1);
          }
           
           JsonArray array = ja.build();
           
           JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle())
                   .add("font", this.getFont())
                   .add("items", array)
                   .build();
           
           return jso;
       }
    public ArrayList<ListItem> getAllListItems() {
        return allListItems;
    }

    public void setAllListItems(ArrayList<ListItem> allListItems) {
        this.allListItems = allListItems;
    }
    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }
    
}
