/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components.FileSelection;

import static ePortfolioGenerator.StartupConstants.PATH_VIDEO;
import java.io.File;
import javafx.scene.control.Tab;
import javafx.stage.FileChooser;

/**
 *
 * @author ycastro
 */
public class VideoSelectionController {
    

/**
 * This controller provides a controller for when the user chooses to
 * select an image for the slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class ImageSelectionController {
    
    /**
     * Default contstructor doesn't need to initialize anything
     */
//    public ImageSelectionController(SlideShowMakerView initUi) {   
//	ui = initUi;
//    }
//    
    /**
     * This function provides the response to the user's request to
     * select an image.
     * 
     * @param slideToEdit - Slide for which the user is selecting an image.
     * 
     * @param view The user interface control group where the image
     * will appear after selection.
     */
    public void processSelectVideo(Tab TabToEdit) {
	FileChooser videoFileChooser = new FileChooser();
	
	// SET THE STARTING DIRECTORY
	videoFileChooser.setInitialDirectory(new File(PATH_VIDEO));
	
	// LET'S ONLY SEE IMAGE FILES
	FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("MP3 files (*.jpg)", "*.MP3");
	FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("MP4 files (*.png)", "*.MP4");
	FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
	videoFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);
	
	// LET'S OPEN THE FILE CHOOSER
	File file = videoFileChooser.showOpenDialog(null);
	if (file != null) {
	    String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
	    String fileName = file.getName();
	    //slideToEdit.setImage(path, fileName);
	    //view.updateSlideImage();
	    //ui.updateFileToolbarControls(false);
	}
    }
}
    
}
