/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonObject;
import jdk.nashorn.internal.codegen.CompilerConstants;

/**
 *
 * @author Yormax
 */
public abstract class Component extends HBox {
      private Button Remover = new Button ("Remove");
      VBox RemoveBox = new VBox(getRemover());

   

    public void setRemoveBox(VBox RemoveBox) {
        this.RemoveBox = RemoveBox;
    }
     public VBox getRemoveBox() {
        return RemoveBox;
    }
      
     private String title = new String();
     String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
      private HBox Content;
          private Button EditButton = new Button("Edit");

    public void setTitle(String title)
    {
    this.title = title;
    
    }
    
     /*public  JsonObject saveToJson(){
        return Json.createObjectBuilder().build();
     }*/
 
public abstract JsonObject saveToJson();
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the Content
     */
    public HBox getContent() {
        return Content;
    }

    /**
     * @param Content the Content to set
     */
    public void setContent(HBox Content) {
        this.Content = Content;
    }

    /**
     * @return the EditButton
     */
    public Button getEditButton() {
        return EditButton;
    }

    /**
     * @param EditButton the EditButton to set
     */
    public void setEditButton(Button EditButton) {
        this.EditButton = EditButton;
    }
    public void getInformation()
    {
    System.out.println(this.title);
    
    }

    /**
     * @return the Remove
     */
    public Button getRemover() {
        return Remover;
    }

    /**
     * @param Remove the Remove to set
     */
    public void setRemover(Button Remove) {
        this.Remover = Remove;
    }
    
   
    
    
}
