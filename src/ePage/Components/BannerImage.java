/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import static ePortfolioGenerator.StartupConstants.IMAGE_COMPONENT_IMG;
import static ePortfolioGenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;
import java.math.BigDecimal;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class BannerImage extends BorderPane {
    
    Button ChoseButton;
    Label CustomWidth;
    Label CustomHeight;
    private TextField widthInput;
    private TextField heightInput;
    VBox Specs;
    HBox widthBox;
    HBox heightBox;    
    File image ;
    private Image im1;
    ImageView iv1;
    FileChooser ImageChooser;
    Button chooseImage;
    private String FilePath;

    public TextField getWidthInput() {
        return widthInput;
    }

    public TextField getHeightInput() {
        return heightInput;
    }

    public String getFilePath() {
        return FilePath;
    }
    Stage ImageStage ;
    public BannerImage(){
    makeBannerImage();
    }
    public void makeBannerImage(){
        CustomWidth = new Label("Width : ");
        CustomHeight = new Label("Height : ");
        setWidthInput(new TextField("500"));
        setHeightInput(new TextField("500"));
        
        heightBox = new HBox(CustomHeight, getHeightInput());
        widthBox = new HBox(CustomWidth, getWidthInput());
        Specs = new VBox(heightBox,widthBox);
        
        chooseImage = new Button("Choose Image");
        chooseImage.setAlignment(Pos.BOTTOM_RIGHT);
            setIm1(new Image(IMAGE_COMPONENT_IMG));            
            iv1 =  new ImageView(getIm1());
            iv1.setFitHeight(100);
            iv1.setFitWidth(100);

        
         
      chooseImage.setOnAction(e -> {
          
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images Files", "*.JPG", "*.JPEG"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                   if(getWidthInput().getText().isEmpty()|getHeightInput().getText().isEmpty()
                        |!isDigit(widthInput.getText())
                        |!isDigit(heightInput.getText()))
                {
                
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
            alert.setContentText("A textfield has an invalid input please check !!");

            alert.showAndWait();
                
                }
                
                else{
                       this.setFilePath(selectedFile.getAbsolutePath());
              this.image =  selectedFile;
              Image im2 = new Image("file:"+selectedFile.getAbsolutePath());
              iv1.setImage(im2);
                        }
        
           
	    
            

            }
            

        }
        );
      
      this.setRight(Specs);
      this.setLeft(iv1);
    
      this.setBottom(chooseImage);
    }
    public void getBannerImageInformation()
   {
       System.out.println(this.getFilePath());

       
   
   
   }
  
       private boolean isDigit(String string)
    { 
        
        if (string == null || string.isEmpty()) {
        return false;
    }
  
    for (int i = 0; i < string.length(); i++) {
        
        if (!Character.isDigit(string.charAt(i))) {
            return false;
        }
    }
    
    
    return true;
        
    
    }

    /**
     * @param widthInput the widthInput to set
     */
    public void setWidthInput(TextField widthInput) {
        this.widthInput = widthInput;
    }

    /**
     * @param heightInput the heightInput to set
     */
    public void setHeightInput(TextField heightInput) {
        this.heightInput = heightInput;
    }

    /**
     * @return the im1
     */
    public Image getIm1() {
        return im1;
    }

    /**
     * @param im1 the im1 to set
     */
    public void setIm1(Image im1) {
        this.im1 = im1;
    }

    /**
     * @param FilePath the FilePath to set
     */
    public void setFilePath(String FilePath) {
        this.FilePath = FilePath;
    }

    public ImageView getIv1() {
        return iv1;
    }

    public void setIv1(ImageView iv1) {
        this.iv1 = iv1;
    }
    
      
    
}
