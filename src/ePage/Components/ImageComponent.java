/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import static ePortfolioGenerator.StartupConstants.IMAGE_COMPONENT_IMG;
import static ePortfolioGenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class ImageComponent extends Component {
     Label CaptionLabel = new Label ("Caption : ");
     TextField CaptionInput = new TextField(" ");
     HBox CaptionBox = new HBox(CaptionLabel,CaptionInput);
     
    Label NameLabel;
        TextField width;
        TextField height;
        TextField NameField;
        HBox NameBox;
        Button SetButton;
        Stage ImageStage; 
        BorderPane root = new BorderPane();
        ImageView iv1 ;

        
//
    private String ImagePath ;
    private File Image;
    private String CustomWidth ; 
    private String CustomHeight;
    private String Caption;
        
    public ImageComponent()
    {
    setImageComponent();
    }

    public ImageComponent(String title, String caption, String height, String width, String imagePath) 
    {
            setImageComponent();
            this.setTitle(title);
            this.CustomHeight = height;
            this.CustomWidth = width;
            this.ImagePath = imagePath;
            this.NameField.setText(title);
            this.CaptionInput.setText(caption);
            this.width.setText(width);
            this.height.setText(height);
            this.Image=new File(ImagePath);
             Image im2 = new Image("file:"+ImagePath);
              iv1.setImage(im2);                

    }
    
    
        public void setImageComponent() {
       
       this.setType("image");
        NameBox = new HBox();
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        NameLabel = new Label("Title of The Image: ");
        NameField = new TextField();
        NameBox.getChildren().addAll(NameLabel, NameField);
        NameBox.setSpacing(5.0);
        NameBox.setAlignment(Pos.CENTER);

        HBox ButtonBox = new HBox();
        Button ChooseVideoBtn = new Button("Choose Image");
        Image im1 = new Image(IMAGE_COMPONENT_IMG);        
        iv1 = new ImageView(im1);
        iv1.setFitHeight(100);
        iv1.setFitWidth(100);

        VBox Specs = new VBox();
        HBox WidthBx = new HBox();
        HBox HeightBox = new HBox();
        
        Label ask1 = new Label("Width:");
        ask1.setPrefWidth(50);
        Label ask2 = new Label("Height:");
        ask2.setPrefWidth(50);

         width = new TextField("");
         height = new TextField("");
        WidthBx.getChildren().addAll(ask1, width);
        HeightBox.getChildren().addAll(ask2, height);

        Specs.getChildren().addAll(HeightBox, WidthBx,CaptionBox);
        Specs.setAlignment(Pos.CENTER);

        
        ChooseVideoBtn.setPrefWidth(100.0);

        SetButton = new Button("Set");
        SetButton.setPrefWidth(100.0);
        SetButton.setOnAction(e->
        {
            if(height.getText().isEmpty()|width.getText().isEmpty()
                        |NameField.getText().isEmpty()
                        |!isDigit(height.getText())
                        |!isDigit(width.getText()))
                {
                
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
            alert.setContentText("A textfield has an invalid input please check !!");

            alert.showAndWait();
                
                }
             else{
           this.setCustomWidth(width.getText()); 
           this.setCustomHeight(height.getText()); 
           this.setTitle(NameField.getText());
           this.setCaption(CaptionInput.getText());
            }
        
        }
        );

        ButtonBox.getChildren().add(ChooseVideoBtn);
        ButtonBox.getChildren().add(SetButton);
        ButtonBox.setAlignment(Pos.CENTER);
       ChooseVideoBtn.setOnAction(e -> {
            ImageStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.JPEG", "*.JPG"));
            File selectedFile = fileChooser.showOpenDialog(ImageStage);
            if (selectedFile != null) {
             
             Image im2 = new Image("file:"+selectedFile.getAbsolutePath());
              iv1.setImage(im2);    
         
           this.setImagePath(selectedFile.getAbsolutePath()/*.substring(0, selectedFile.getPath().indexOf(selectedFile.getName()))*/);
           this.setImage(selectedFile);
                   
	    
            

            }
            

        }
        );

        root = new BorderPane();
        root.setStyle("-fx-background-color: aqua;");
        root.setTop(NameBox);
        root.setCenter(Specs);
        root.setRight(this.getRemoveBox());
        this.RemoveBox.setAlignment(Pos.TOP_RIGHT);
        root.setLeft(iv1);
        root.setBottom(ButtonBox);
        this.getChildren().add(root);

    }
 public ImageComponent getImgComponent()
 {
     return this;
 }
    /**
     * @return the ImagePath
     */
    public String getImagePath() {
        return ImagePath;
    }

    /**
     * @param ImagePath the ImagePath to set
     */
    public void setImagePath(String ImagePath) {
        this.ImagePath = ImagePath;
    }

    /**
     * @return the CustomWidth
     */
    public String getCustomWidth() {
        return CustomWidth;
    }

    /**
     * @param CustomWidth the CustomWidth to set
     */
    public void setCustomWidth(String CustomWidth) {
        this.CustomWidth = CustomWidth;
    }

    /**
     * @return the CustomHeight
     */
    public String getCustomHeight() {
        return CustomHeight;
    }

    /**
     * @param CustomHeight the CustomHeight to set
     */
    public void setCustomHeight(String CustomHeight) {
        this.CustomHeight = CustomHeight;
    }

    /**
     * @return the Image
     */
    public File getImage() {
        return Image;
    }

    /**
     * @param Image the Image to set
     */
    public void setImage(File Image) {
        this.Image = Image;
    }
    
    public void getInformation()
    {
        System.out.println(this.getTitle()+" "+ImagePath+" Width:"+CustomWidth+
                "Height: "+CustomHeight );
    
    }
       private boolean isDigit(String string)
    { 
        
        if (string == null || string.isEmpty()) {
        return false;
    }
  
    for (int i = 0; i < string.length(); i++) {
        
        if (!Character.isDigit(string.charAt(i))) {
            return false;
        }
    }
    
    
    return true;
        
    
    }

    /**
     * @return the Caption
     */
    public String getCaption() {
        return Caption;
    }

    /**
     * @param Caption the Caption to set
     */
    public void setCaption(String Caption) {
        this.Caption = Caption;
    }
      public JsonObject saveToJson(){
                JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle() )
                   .add("caption",this.getCaption() )
                   .add("height",this.getCustomHeight() )
                   .add("width",this.getCustomWidth() )
                   .add("filepath",this.getImagePath() )
                   .build();
           
           
           return jso;
       }
    
    
    
    
}
