/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import static ePortfolioGenerator.StartupConstants.IMAGE_COMPONENT_IMG;
import static ePortfolioGenerator.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Ycastro
 */
public class Slide extends BorderPane {
    //BorderPane Bp = new BorderPane();

    Label CaptionLabel = new Label("Caption : ");
    TextField CaptionInput = new TextField(" ");
    Button setCaptionBtn = new Button("Set Caption");
    HBox CaptionBox = new HBox(CaptionLabel, CaptionInput, setCaptionBtn);
    

    Button ChooseBtn = new Button("Choose Image");
    Image im1;
    ImageView iv1;

    //Actual Content 
    private int index;
    private boolean Selected = false;
    private String FilePath;
    private String Caption = new String();
    
    private SlideShowScrollPane parent;
    
    
    
     Slide(SlideShowScrollPane parent ,  String FilePath, String Caption , int index) {
     
     
     
        this.parent = parent;
        this.setOnMouseClicked(e->{
            parent.clearSelectedSlide();
            this.setStyle("-fx-background-color: red;");
            this.Selected = true;
            
            parent.setSelectedSlide(this);
            
        });
        
        this.setStyle("-fx-background-color: yellow;");
        im1 = new Image(IMAGE_COMPONENT_IMG);
        iv1 = new ImageView(im1);

        iv1.setFitHeight(100);
        iv1.setFitWidth(100);

        ChooseBtn.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.JPEG", "*.JPG", "*.PNG"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {

                this.im1 = new Image("file:" + selectedFile.getAbsolutePath());
                this.FilePath = "file:" + selectedFile.getAbsolutePath();

               // iv1.setImage(im1);

            }

        }
        );

        iv1.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.JPEG", "*.JPG", "*.PNG"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {

                this.im1 = new Image("file:" + selectedFile.getAbsolutePath());
                this.FilePath = "file:" + selectedFile.getAbsolutePath();
                iv1.setImage(im1);

            }

        });
        setCaptionBtn.setOnAction(e -> {
            this.Caption = CaptionInput.getText();
        }
        );
        // setCaptionBtn.setOnAction
        //this.getChildren().add(Bp);
     this.FilePath = FilePath;
     this.Caption = Caption;
     this.CaptionInput.setText(Caption);
     this.index = index;
     this.parent = parent;
    this.im1 = new Image(FilePath);
    this.iv1.setImage(im1);

        this.setLeft(iv1);
        this.setCenter(CaptionBox);
    }

    public Slide(SlideShowScrollPane parent) {
        this.parent = parent;
        this.setOnMouseClicked(e->{
            parent.clearSelectedSlide();
            this.setStyle("-fx-background-color: red;");
            this.Selected = true;
            
            parent.setSelectedSlide(this);
            
        });
        
        this.setStyle("-fx-background-color: yellow;");
        im1 = new Image(IMAGE_COMPONENT_IMG);
        iv1 = new ImageView(im1);

        iv1.setFitHeight(100);
        iv1.setFitWidth(100);

        ChooseBtn.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.JPEG", "*.JPG", "*.PNG"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {

                this.im1 = new Image("file:" + selectedFile.getAbsolutePath());
                FilePath = "file:" + selectedFile.getAbsolutePath();

               // iv1.setImage(im1);

            }

        }
        );

        iv1.setOnMouseClicked(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.JPEG", "*.JPG", "*.PNG"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {

                this.im1 = new Image("file:" + selectedFile.getAbsolutePath());
                FilePath = "file:" + selectedFile.getAbsolutePath();
                iv1.setImage(im1);

            }

        });
        setCaptionBtn.setOnAction(e -> {
            this.Caption = CaptionInput.getText();
        }
        );
        // setCaptionBtn.setOnAction
        this.setLeft(iv1);
        this.setCenter(CaptionBox);
        //this.getChildren().add(Bp);
    }


    public void getInformation() {
        System.out.println(this.getFilePath() + " Caption:" + CaptionInput.getText());

    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the FilePath
     */
    public String getFilePath() {
        return FilePath;
    }

    /**
     * @param FilePath the FilePath to set
     */
    public void setFilePath(String FilePath) {
        this.FilePath = FilePath;
    }

    /**
     * @return the Caption
     */
    public String getCaption() {
        return Caption;
    }

    /**
     * @param Caption the Caption to set
     */
    public void setCaption(String Caption) {
        this.Caption = Caption;
    }

    /**
     * @return the Selected
     */
    public boolean isSelected() {
        
        return Selected;
    }

    /**
     * @param Selected the Selected to set
     */
    public void setSelected(boolean Selected) {
        
        this.Selected = Selected;
    }

}
