/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class HeaderComponent extends Component  {
      VBox NameBox;
      Label NameLabel;
      TextField NameField;
      Button SetNameButton;
      BorderPane root;
      
      
    public HeaderComponent(){
    MakeHeaderBox();
    }

    public HeaderComponent(String content) {
        MakeHeaderBox();
        this.NameField.setText(content);
        this.setTitle(content);
        
    }
      public void MakeHeaderBox() {
          this.setType("header");
        VBox NameBox = new VBox();
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        NameLabel = new Label("Please enter the Header: ");
        NameField = new TextField();
        NameField.setPrefWidth(100);
        SetNameButton = new Button("Set Header");
        NameBox.getChildren().addAll(NameLabel, NameField, SetNameButton);
        NameBox.setSpacing(5.0);
        NameBox.setAlignment(Pos.CENTER);
        
        SetNameButton.setOnAction(e ->
        {
        this.setTitle(NameField.getText());
        
        }
        
        );

        root = new BorderPane();
        root.setCenter(NameBox);
        root.setRight(this.getRemoveBox());
        this.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
       this.getChildren().add(root);


      

    }
      
       public void getInformation()
    {
    System.out.println(this.getTitle() );
    
    }
   public JsonObject saveToJson(){
                JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle() )
                   .build();
           
           
           return jso;
       }
    
    
}
