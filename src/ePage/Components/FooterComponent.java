/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

import java.math.BigDecimal;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class FooterComponent extends Component {
    
          HBox NameBox;
      Label NameLabel;
      TextArea NameField;
      Button SetNameButton;
      BorderPane root;
       
      
      
    public FooterComponent(){
    MakeFooterBox();
    }
    
    public FooterComponent(String content){
    MakeFooterBox();
    this.setType("Footer");
    NameField.setText(content);

    
    }
      public void MakeFooterBox() {        
        NameBox = new HBox();
        this.setType("Footer");
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        NameLabel = new Label("Please enter the Footer: ");
        NameField = new TextArea();
        NameField.setPrefWidth(100);
        NameField.setPrefHeight(100);
        SetNameButton = new Button("Set Footer");
        NameBox.getChildren().addAll(NameLabel, NameField, SetNameButton);
        NameBox.setSpacing(5.0);
        NameBox.setAlignment(Pos.CENTER);
        
        SetNameButton.setOnAction(e ->
        {
        this.setTitle(NameField.getText());
        
        }
        
        );

        root = new BorderPane();
        root.setRight(this.getRemoveBox());
        this.RemoveBox.setAlignment(Pos.TOP_RIGHT);
        root.setCenter(NameBox);
       this.getChildren().add(root);


      

    }
      public void getInformation()
      {

    System.out.println(this.getTitle());
    System.out.print(NameField.getText());
    
    
      
      }
       public JsonObject saveToJson(){
                JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle() )
                   .build();
           
           
           return jso;
       }
      
    
}
