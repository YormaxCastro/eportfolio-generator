/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ePage.Components;

import static ePortfolioGenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.ICON_ADD_SLIDE;
import static ePortfolioGenerator.StartupConstants.ICON_EXIT;
import static ePortfolioGenerator.StartupConstants.ICON_MOVE_DOWN;
import static ePortfolioGenerator.StartupConstants.ICON_MOVE_UP;
import static ePortfolioGenerator.StartupConstants.ICON_REMOVE_SLIDE;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ycastro
 */
public class SlideShowScrollPane extends BorderPane {
    
            
            
            
    Label slidesLabel = new Label("Slides");
    VBox allSlideVBox = new VBox();
    
        
    VBox slideToolBar = new VBox();
    Button addBtn;
    Button removeBtn;
    Button moveUpBtn ;
    Button moveDownBtn;
    
    
    //content
    ArrayList<Slide> allSlide = new ArrayList<Slide>();
        Slide firstSlide;


   
    int sizeOfSlides = 0; 
    Slide selectedSlide = null; 
    
    
    
    
    
     public ArrayList<Slide> getAllSlide() {
        return allSlide;
    }
    public SlideShowScrollPane(){     
    
    slideToolBar.setAlignment(Pos.CENTER);
    addBtn = initChildButton(slideToolBar, ICON_ADD_SLIDE,"Add",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    removeBtn = initChildButton(slideToolBar, ICON_REMOVE_SLIDE,"Remove",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    moveUpBtn = initChildButton(slideToolBar, ICON_MOVE_UP,"Move up",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    moveDownBtn = initChildButton(slideToolBar, ICON_MOVE_DOWN,"Move Down",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
     
     firstSlide = new Slide(this);
    firstSlide.setIndex(0);
    allSlide.add(firstSlide);
    sizeOfSlides++;
    allSlideVBox.getChildren().add(firstSlide);
    initButtons();
    this.setCenter(allSlideVBox);
    //this.setRight(slideToolBar);
    this.setTop(slidesLabel);

    
    
    
    }
    public void clearSelectedSlide( )
    {
         for( Slide i : allSlide)
          {
                   i.setSelected(false);
                   i.setStyle("-fx-background-color: yellow;");
                 }
    
    
    }
    
    
      public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
            String tooltip,
	    String cssClass,
	    boolean disabled) {
          
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
        
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
        button.setTooltip(new Tooltip(tooltip));

	toolbar.getChildren().add(button);

	return button;
    }
public void initButtons()
{
 addBtn.setOnAction(e->
    {
       sizeOfSlides++;

  Slide newSlide = new Slide(this);
    newSlide.setIndex(sizeOfSlides);
    
    allSlide.add(newSlide);
    allSlideVBox.getChildren().add(newSlide);
    
 
        }
         
        );
 removeBtn.setOnAction(e->
 {
     if (sizeOfSlides != 0)
     {
         System.out.println("removing :" + selectedSlide.getIndex());
    sizeOfSlides--;
    allSlide.remove(selectedSlide);
    allSlideVBox.getChildren().remove(selectedSlide);
    for( int i = 0 ; i < allSlide.size();i++ )
    {
        allSlide.get(i).setIndex(i);
    
    }
     }
 }
 );
 moveUpBtn.setOnAction(e-> 
{ 
    //Fix me and Add the Other Button Functionality 
    //Button moveUpBtn ;
    //Button moveDownBtn;
    if (selectedSlide.getIndex() == 0 || sizeOfSlides == 0)
    {

    }    
    else 
    {
        Slide Temp = selectedSlide;
        int selectedIndex = selectedSlide.getIndex();
        Slide prev = (Slide) allSlideVBox.getChildren().get(selectedIndex - 1);
        Temp.setIndex(selectedIndex - 1);
        prev.setIndex(selectedIndex);
               

        allSlideVBox.getChildren().set(selectedIndex - 1, Temp);
        allSlideVBox.getChildren().set(selectedIndex, prev);
        
        
        
    
    }


}
 );
 moveDownBtn.setOnAction(e-> 
{ 
    //Fix me and Add the Other Button Functionality 
    //Button moveUpBtn ;
    //Button moveDownBtn;
    if (selectedSlide.getIndex() == 0 || sizeOfSlides == 0)
    {

    }    
    else 
    {
        Slide Temp = selectedSlide;
        int selectedIndex = selectedSlide.getIndex();
        Slide next = (Slide) allSlideVBox.getChildren().get(selectedIndex +1);
        Temp.setIndex(selectedIndex +1);
        next.setIndex(selectedIndex);
        allSlideVBox.getChildren().set(selectedIndex +1, Temp);
        allSlideVBox.getChildren().set(selectedIndex, next);
        
        
        
    
    }


}
 );

    

}

    public Slide getSelectedSlide() {
        return selectedSlide;
    }

    public void setSelectedSlide(Slide selectedSlide) {
        this.selectedSlide = selectedSlide;
    }



    public VBox getSlideToolBar() {
        return slideToolBar;
    }

    public void setSlideToolBar(VBox slideToolBar) {
        this.slideToolBar = slideToolBar;
    }
public void getInformation()
{
    for(Slide item : allSlide)
        {
            
		item.getInformation();
	}
}

    
}
