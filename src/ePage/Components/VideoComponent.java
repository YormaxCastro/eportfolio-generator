/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePage.Components;

/**
 *
 * @author ycastro
 */
import static ePortfolioGenerator.StartupConstants.PATH_VIDEO;
import static ePortfolioGenerator.StartupConstants.VIDEO_COMPONENT_IMG;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonObject;

/**
 *
 * @author ycastro
 */
public class VideoComponent extends Component {
        
  
    Label NameLabel;
        TextField NameField;
        HBox NameBox;
        Button SetButton;
        Stage VideoStage; 
        TextField width;
        TextField height;
        BorderPane root = new BorderPane();
        
         Label CaptionLabel = new Label ("Caption : ");
     TextField CaptionInput = new TextField(" ");
     HBox CaptionBox = new HBox(CaptionLabel,CaptionInput);
        
//
    private String VideoPath ;
    private String Caption;
    private String CustomWidth ; 
    private String CustomHeight; 
    private File Video;
        
    public VideoComponent()
    {
    setVideoComponent();
    }

    public VideoComponent(String title, String filePath, String caption, String width, String height) {
        
    setVideoComponent();
    this.setTitle(title);
    this.VideoPath = filePath;
    this.Caption = Caption;
    this.CustomHeight = height;
    this.CustomWidth = width;
    this.NameField.setText(title);
    this.CaptionInput.setText(caption);
    this.width.setText(width);
    this.height.setText(height);
    
    }
    
    
        public void setVideoComponent() {
        this.setType("video");
        NameBox = new HBox();
        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        NameLabel = new Label("Title of The Video: ");
        NameField = new TextField();
        NameBox.getChildren().addAll(NameLabel, NameField);
        NameBox.setSpacing(5.0);
        NameBox.setAlignment(Pos.CENTER);

        HBox ButtonBox = new HBox();
        Button ChooseVideoBtn = new Button("Choose Video");
        Image im1 = new Image(VIDEO_COMPONENT_IMG);        
        ImageView iv1 = new ImageView(im1);
        iv1.setFitHeight(100);
        iv1.setFitWidth(100);

        VBox Specs = new VBox();
        HBox WidthBx = new HBox();
        HBox HeightBox = new HBox();
        Label ask1 = new Label("Width:");
        ask1.setPrefWidth(50);
        Label ask2 = new Label("Height:");
        ask2.setPrefWidth(50);

         width = new TextField("");
         height = new TextField("");
        WidthBx.getChildren().addAll(ask1, width);
        HeightBox.getChildren().addAll(ask2, height);
        setCaption(new String());

        Specs.getChildren().addAll(HeightBox, WidthBx,CaptionBox);
        Specs.setAlignment(Pos.CENTER);

        
        ChooseVideoBtn.setPrefWidth(100.0);

        SetButton = new Button("Set");
        SetButton.setPrefWidth(100.0);
        
        SetButton.setOnAction(e->
        {
        if(height.getText().isEmpty()|width.getText().isEmpty()
                        |NameField.getText().isEmpty()
                        |!isDigit(height.getText())
                        |!isDigit(width.getText()))
                {
                
                Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
            alert.setContentText("A textfield has an invalid input please check !!");

            alert.showAndWait();
                
                }
                else{
                
           this.setCustomWidth(width.getText()); 
           this.setCustomHeight(height.getText()); 
           this.setTitle(NameField.getText());
           this.setCaption(CaptionInput.getText());
                }
        
        
        }
        );

        ButtonBox.getChildren().add(ChooseVideoBtn);
        ButtonBox.getChildren().add(SetButton);
        ButtonBox.setAlignment(Pos.CENTER);
       ChooseVideoBtn.setOnAction(e -> {
           
            VideoStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
//            fileChooser.setInitialDirectory(new File(PATH_VIDEO));

            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Video Files", "*.MP4", "*.MP3"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
                this.setVideoPath(selectedFile.getAbsolutePath());
                
           
	    
            

            }
            

        }
        );

        root = new BorderPane();
        root.setStyle("-fx-background-color: aqua;");
        root.setTop(NameBox);
        root.setCenter(Specs);
        root.setLeft(iv1);
        root.setBottom(ButtonBox);
        
        root.setRight(this.getRemoveBox());
       
        
        
        this.getChildren().add(root);
    }
 public VideoComponent getVideoComponent()
 {
     return this;
 }
    /**
     * @return the VideoPath
     */
    public String getVideoPath() {
        return VideoPath;
    }

    /**
     * @param ImagePath the ImagePath to set
     */
    public void setVideoPath(String VideoPath) {
        this.VideoPath = VideoPath;
    }

    /**
     * @return the CustomWidth
     */
    public String getCustomWidth() {
        return CustomWidth;
    }

    /**
     * @param CustomWidth the CustomWidth to set
     */
    public void setCustomWidth(String CustomWidth) {
        this.CustomWidth = CustomWidth;
    }

    /**
     * @return the CustomHeight
     */
    public String getCustomHeight() {
        return CustomHeight;
    }

    /**
     * @param CustomHeight the CustomHeight to set
     */
    public void setCustomHeight(String CustomHeight) {
        this.CustomHeight = CustomHeight;
    }

    /**
     * @return the Image
     */
    public File getVideo() {
        return Video;
    }

    /**
     * @param Image the Image to set
     */
    public void setVideo(File Video) {
        this.Video = Video;
    }
    
    public void getInformation()
    {
        System.out.println(this.getTitle()+" "+VideoPath+" Width:"+CustomWidth+
                "Height: "+CustomHeight );
    
    }
    private boolean isDigit(String string)
    { 
        
        if (string == null || string.isEmpty()) {
        return false;
    }
  
    for (int i = 0; i < string.length(); i++) {
        
        if (!Character.isDigit(string.charAt(i))) {
            return false;
        }
    }
    
    
    return true;
        
    
    }

    /**
     * @return the Caption
     */
    public String getCaption() {
        return Caption;
    }

    /**
     * @param Caption the Caption to set
     */
    public void setCaption(String Caption) {
        this.Caption = Caption;
    }
     public JsonObject saveToJson(){
                JsonObject jso = Json.createObjectBuilder()
                   .add("type", this.getType())
                   .add("title",this.getTitle() )
                   .add("filepath",this.getVideoPath())
                    .add("caption",this.getCaption())
                    .add("width",this.getCustomWidth())
                    .add("height",this.getCustomHeight())
                    
                   .build();
           
           
           return jso;
       }
    
    
    
    
}


