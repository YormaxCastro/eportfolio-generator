package EPortfolio.View;

import SiteExporter.SiteExporter;
import ePage.Components.BannerComponent;
import ePage.Components.Component;
import ePage.Components.FooterComponent;
import ePage.Components.HeaderComponent;
import ePage.Components.ImageComponent;
import ePage.Components.ListComponent;
import ePage.Components.SlideShowComponent;
import ePage.Components.TextComponent;
import ePage.Components.VideoComponent;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ePortfolioGenerator.StartupConstants.CSS_CLASS_Main_Background;
import static ePortfolioGenerator.StartupConstants.ICON_EXIT;
import static ePortfolioGenerator.StartupConstants.ICON_EXPORT;
import static ePortfolioGenerator.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.ICON_SAVE_AS;
import static ePortfolioGenerator.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ePortfolioGenerator.StartupConstants.PATH_ICONS;
import static ePortfolioGenerator.StartupConstants.PATH_JSON;
import static ePortfolioGenerator.StartupConstants.PATH_VIDEO;
import static ePortfolioGenerator.StartupConstants.STYLE_SHEET_UI;
import ePortfolioObj.ePageModel;
import ePortfolioObj.ePortfolio;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.Tab.*;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;


/**
 *
 * @author Yormax
 */
    

public class EPortfolioMakerViewer {
// The Master ePorfolio 
    ePortfolio Master;
    ePageModel selectedPage = null;
    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;
    Stage dialogBox;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    //The Root of the Whole scene will be the root
    BorderPane ePortfolioRootPane;
    BorderPane EditorPane;
   // TabPane tabPane ;
    


    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newEPortfolioButton;
    Button loadEPortfolioButton;
    Button saveAsEPortfolioButton;
    Button exportEPortfolioButton;
    Button saveEPortfolioButton;
    Button exitButton;
  
    // Button for Comp Toolbar
    Button AddTextComponent;
    Button AddVideoComponent;
    Button AddSSComponent;
    Button AddFooter;
    Button AddListComponent;
    Button AddHeader;
    Button AddImage;
    Button AddBanner;
    Button RemoveComponent;
    Button setStyle;

    
    // WORKSPACE
    VBox TitleBox;
    VBox PageToolBar;
    VBox ButtonBox;//idk         

    
    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    HBox ePageEditToolbar;
    Button addEPageButton;
    Button removeEPageButton;

    
    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    // AND THIS WILL GO IN THE CENTER
    //Here would go the tabpane
    ScrollPane ePageEditorScrollPane;
    VBox ePageEditorPane;
    
    HBox workspaceSwitch;
     HBox NameBox;
   // ScrollPane SelectionsComp;
    
   SingleSelectionModel<ePageModel> SelectedPage ;
   // The List of all Epages
 //ArrayList<ePageModel> ePageModelslist = new ArrayList<ePageModel>();
 // get the selected Tab

   // name fields 
   TextField studentNameInput;
   Button setName ;
   
    //  page layout 
              Label layoutPrompt = new Label("Page Layout : ");
             
         ComboBox<String> myLayoutSelection = new ComboBox<String>();
            VBox layoutSel = new VBox(layoutPrompt,myLayoutSelection);
        // page style
              Label stylePrompt = new Label("Page style : ");
         ComboBox<String> myStyleSelection = new ComboBox<String>();
            VBox StyleSel = new VBox(stylePrompt,myStyleSelection);
            
        VBox pageOption = new VBox(StyleSel,layoutSel);
        
    
            
//            myTextSelection.getItems().addAll("Font 1","Font 2","Font 3","Font 4","Font 5");
//            myTextSelection.getSelectionModel().selectFirst();

    
    
    
    
    
    public EPortfolioMakerViewer(){}
     private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
     

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newEPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,"New Portfolio",   CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadEPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,"Load Portfolio", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW, "Save Portfolio", CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        saveAsEPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_AS,"Save As",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exportEPortfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT,"Export",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT,"Exit",CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);

        
        
	//viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
       fileToolbarPane.setMargin(newEPortfolioButton,new Insets(5,5,5,5));
       fileToolbarPane.setMargin(loadEPortfolioButton,new Insets(5,5,5,5));
       fileToolbarPane.setMargin(saveEPortfolioButton,new Insets(5,5,5,5));
       fileToolbarPane.setMargin(exportEPortfolioButton,new Insets(5,5,5,5));
       fileToolbarPane.setMargin(saveAsEPortfolioButton,new Insets(5,5,5,5));
       fileToolbarPane.setMargin(exitButton,new Insets(5,5,5,5));



        //fileToolbarPane.setMargin(viewSlideShowButton,new Insets(5,5,5,5));



    
    
    }
     public  void initPageToolBar()
     {
         setStyle = new Button("Set Style");
         setStyle.setPrefWidth(150.0);
         pageOption.getChildren().add(setStyle);
          myStyleSelection.getItems().addAll("Style 1","Style 2","Style 3","Style 4","Style 5");
          myStyleSelection.getSelectionModel().selectFirst();
          layoutPrompt.setPrefWidth(150);
          myStyleSelection.setPrefWidth(150);
           
         setStyle = new Button("Add Page");
         setStyle.setPrefWidth(150.0);
         
          myLayoutSelection.getItems().addAll("Layout 1","Layout 2","Layout 3","Layout 4","Layout 5");
          myLayoutSelection.getSelectionModel().selectFirst();
          stylePrompt.setPrefWidth(150);
          myLayoutSelection.setPrefWidth(150);


         PageToolBar = new VBox();
         Label SiteToolBar = new Label("Page ToolBar");
         addEPageButton = new Button("Add Page");
         addEPageButton.setPrefWidth(150.0);
         
         removeEPageButton = new Button("Remove Page");
         removeEPageButton.setPrefWidth(150.0);
         
         Label ToolBar = new Label("Site ToolBar");
         PageToolBar.setAlignment(Pos.CENTER);
         
        AddTextComponent = new Button("Add Text Component");
         AddTextComponent.setPrefWidth(150.0);
         
         AddVideoComponent = new Button("Add Video Component");
         AddVideoComponent.setPrefWidth(150.0);
         
         AddImage = new Button("Add an Image Component");
         AddImage.setPrefWidth(150.0);
         
         AddSSComponent = new Button("Add Slide Show Component");
         AddSSComponent.setPrefWidth(150.0);
         
          AddFooter = new Button("Add a Page Footer");
         AddFooter.setPrefWidth(150.0);
         
          AddListComponent = new Button("Add a List Component");
         AddListComponent.setPrefWidth(150.0);
         
          AddHeader = new Button("Add a Header");
         AddHeader.setPrefWidth(150.0);
         
          AddBanner = new Button("Add  a Banner ");
         AddBanner.setPrefWidth(150.0);
         
         RemoveComponent = new Button("Remove Component");
         RemoveComponent.setPrefWidth(150.0);
         
         
         PageToolBar.getChildren().add(SiteToolBar);
         PageToolBar.getChildren().add(addEPageButton);
         PageToolBar.getChildren().add(removeEPageButton);
         PageToolBar.getChildren().add(ToolBar);
         PageToolBar.getChildren().add(AddTextComponent);
         PageToolBar.getChildren().add(AddVideoComponent);
         PageToolBar.getChildren().add(AddImage);
         PageToolBar.getChildren().add(AddSSComponent);
         PageToolBar.getChildren().add(AddHeader);
         PageToolBar.getChildren().add(AddFooter);
         PageToolBar.getChildren().add(AddBanner);
         PageToolBar.getChildren().add(AddListComponent);
         PageToolBar.getChildren().add(RemoveComponent);
         PageToolBar.getChildren().add(pageOption);
         
         
        PageToolBar.setMargin(AddTextComponent,new Insets(5,5,5,5));
        PageToolBar.setMargin(AddImage,new Insets(5,5,5,5));

        PageToolBar.setMargin(AddBanner,new Insets(5,5,5,5));
        PageToolBar.setMargin(AddHeader,new Insets(5,5,5,5));
        PageToolBar.setMargin(AddVideoComponent,new Insets (5,5,5,5));
        PageToolBar.setMargin(AddListComponent,new Insets(5,5,5,5));
        PageToolBar.setMargin(AddSSComponent,new Insets(5,5,5,5));
        PageToolBar.setMargin(AddFooter,new Insets(5,5,5,5));
        PageToolBar.setMargin(removeEPageButton,new Insets(5,5,5,5));
        PageToolBar.setMargin(addEPageButton,new Insets(5,5,5,5));


     
     } 
    public void initWorkspaceSwitch()
     {
         
  
         
         
         
         workspaceSwitch = new HBox();
         
         Button WebViewer = new Button("Launch WebViewer");
         WebViewer.setPrefWidth(120.0);
         Button EditorViewer = new Button("Launch Editor ");
         EditorViewer.setPrefWidth(120.0);
        
         WebViewer.setOnAction(e ->
         {
           
         
         }
         
         );
         workspaceSwitch.getChildren().addAll(WebViewer, EditorViewer);
         workspaceSwitch.setAlignment(Pos.BOTTOM_RIGHT);
         
     
     
     
     }
         public void initTitleToolBar()
     {
         TitleBox = new VBox();

        Label label1 = new Label("Name of The Student :");
        
        studentNameInput = new TextField ();
        setName = new Button("Set Your Name");
        setName.setMaxWidth(200.0);
        
        NameBox = new HBox();
        NameBox.setAlignment(Pos.CENTER);

        NameBox.getChildren().addAll(label1, studentNameInput,setName);
        NameBox.setSpacing(10);
        TitleBox.getChildren().add(NameBox);
        
        Label label2 = new Label("Name of The WebPage :");

        TextField textField2 = new TextField ();
        
        Button setPageName = new Button("Set Page Name");
        setPageName.setMaxWidth(200.0);
        
//        HBox hbox2 = new HBox();
//        hbox2.getChildren().addAll(label2, textField2,setPageName);
//        hbox2.setAlignment(Pos.CENTER);
//        hbox2.setSpacing(10);
        
//        TitleBox.getChildren().add(hbox2);
//        TitleBox.setMargin(NameBox,new Insets(5,5,5,5));
//        TitleBox.setMargin(hbox2,new Insets(5,5,5,5));


       
            //VB = new VBox();
            //vbox.getChildren().add(AddComponent);
            //vbox.setAlignment(Pos.CENTER);
           
     
     }
       public void initTabPane()
       { 
       
            
        //tabPane = new TabPane();
       Master = new ePortfolio(); 
        Master.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);        
         }

      public void initEditor()
   {
       EditorPane = new BorderPane(); 
//       EditorPane.setStyle("-fx-background-color: aqua;");
        
       }
     public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
            String tooltip,
	    String cssClass,
	    boolean disabled) {
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
        
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
        button.setTooltip(new Tooltip(tooltip));

	toolbar.getChildren().add(button);
        //Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));

	return button;
    }
      
      
      public void initAllToolbars()
      {
        initFileToolbar();
        initPageToolBar();
        initWorkspaceSwitch();
        initTabPane();
        initTitleToolBar();
      
      
      }
       public void activateEventHandlers()
       {
             
         
          AddBanner.setOnAction(e ->
        {
            
            ActionHbox DBox = new ActionHbox();
            
            ePageModel CurrentTab = (ePageModel) Master.getSelectionModel().getSelectedItem();
            
            
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            BannerComponent Banner = DBox.MakeBanner(CurrentTab.getComponentArrayList(),MasterContent);
            
            
            
           MasterContent.getChildren().add(Banner);
         
         
        
        }
     );
        
     AddTextComponent.setOnAction(e ->
        {
            
            ActionHbox DBox = new ActionHbox();
            
            ePageModel CurrentTab = (ePageModel) Master.getSelectionModel().getSelectedItem();
            
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            TextComponent contMakeTextBox= DBox.MakeTextComponent(CurrentTab.getComponentArrayList(),MasterContent);
            
            
            
            
            
            
           MasterContent.getChildren().add(contMakeTextBox);
                 

        
        }
     );
      AddVideoComponent.setOnAction(e ->
        {
            ActionHbox DBox = new ActionHbox();
            
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            VideoComponent contMakeVideoBox = DBox.MakeVideoComponent(CurrentTab.getComponentArrayList(),MasterContent);
            
            
                   MasterContent.getChildren().add(contMakeVideoBox);
                  // CurrentTab.addToArrayList(contMakeVideoBox);

            
        
        }
     );
       AddImage.setOnAction(e ->
        {
            ActionHbox DBox = new ActionHbox();
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
             ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
          ImageComponent ImgComp =  DBox.MakeImage(CurrentTab.getComponentArrayList(), MasterContent);
            
           MasterContent.getChildren().add(ImgComp);
         // CurrentTab.addToArrayList(ImgComp);

    
        }
    );
    AddHeader.setOnAction(e ->
            {
         
            ActionHbox DBox = new ActionHbox();
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            HeaderComponent HeaderComp =  DBox.MakeHeader(CurrentTab.getComponentArrayList(),MasterContent);
            
           MasterContent.getChildren().add(HeaderComp);
                              // CurrentTab.addToArrayList(HeaderComp);

    
            }   
    
            );
    AddFooter.setOnAction(e->
    {
      ActionHbox DBox = new ActionHbox();
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            FooterComponent FooterComp =  DBox.MakeFooter(CurrentTab.getComponentArrayList(),MasterContent);
            
           MasterContent.getChildren().add(FooterComp);
          // CurrentTab.addToArrayList(FooterComp);

    
    }
    
    );
   
      AddSSComponent.setOnAction(e ->
        {
            ActionHbox DBox = new ActionHbox();
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            SlideShowComponent SSComp =  DBox.MakeSSComponent(CurrentTab.getComponentArrayList(),MasterContent);
            
           MasterContent.getChildren().add(SSComp);
          // CurrentTab.addToArrayList(SSComp);
        }
    );
      AddListComponent.setOnAction(e ->
      {
            ActionHbox DBox = new ActionHbox();
            ePageModel CurrentTab =(ePageModel)Master.getSelectionModel().getSelectedItem();
            ScrollPane theScroll =  (ScrollPane)CurrentTab.getContent();
            VBox MasterContent = (VBox)theScroll.getContent();
            ListComponent ListComp =  DBox.MakeList(CurrentTab.getComponentArrayList(),MasterContent);
            
           MasterContent.getChildren().add(ListComp);
           //CurrentTab.addToArrayList(ListComp);

            
    
      
      
      }
      
      );
        addEPageButton.setOnAction(e -> 
         {
          ePageModel newePage = new ePageModel(Master);
         ActionDialogsBox DBox = new ActionDialogsBox(dialogBox);
         DBox.MakeAddEPage(newePage);
         
                if (newePage.getText().isEmpty())
                {
                    newePage = null;


         
                 }
                else
                {
                       Master.getTabs().add(newePage);
                       //ePageModelslist.add(newePage);
                       //Master.addPage(newePage);

                }
                      reloadToolbar();

         }
         
         
         );
       removeEPageButton.setOnAction(e -> 
       {
       if(Master.getAllPages().size()>0){
           
           
        
      ePageModel CurrentTab = (ePageModel) Master.getSelectionModel().getSelectedItem();
          
     
       
       //ePageModelslist.remove(CurrentTab);
       Master.getTabs().remove(CurrentTab);
    
       }
       
       }
       );
       
         RemoveComponent.setOnAction(e->
           {
          //ePageModel CurrentTab = (ePageModel) tabPane.getSelectionModel().getSelectedItem();
            
               for(ePageModel page : Master.getAllPages()){
           //CurrentTab.getAllData();
            
           page.saveToJson();
           //CurrentTab.saveToJson();
           
           }
               Master.setPortfolioData();
           }
           );
       

        
        
           setStyle.setOnAction( e-> 
           {
           ePageModel CurrentTab = (ePageModel) Master.getSelectionModel().getSelectedItem();
           
           CurrentTab.setMyStyle("Hello");
           CurrentTab.setMyLayout(myLayoutSelection.getSelectionModel().getSelectedItem());

           
           
           } );

    
     
     
     }
       public void activeToolbarHandlers()
       {
          setName.setOnAction(e->{
          Master.setStudentName(studentNameInput.getText());
          
          });
           
            loadEPortfolioButton.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            fileChooser.setInitialDirectory(new File(PATH_JSON));

            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON", "*.JSON"));
            File selectedFile = fileChooser.showOpenDialog(null);
            if (selectedFile != null) {
              Master.LoadJson(selectedFile);
              //Master = 
       
       
       
       
                }
            });
            
            
            
                    
            newEPortfolioButton.setOnAction(e -> {
           
              Master = new ePortfolio(); 
              EditorPane.setCenter(Master);

                     }
                
            );
            
             saveEPortfolioButton.setOnAction(e->
           {
            
               for(ePageModel page : Master.getAllPages()){
            
                    page.saveToJson();
           
                }
               Master.setPortfolioData();
           }
           );
             
             saveAsEPortfolioButton.setOnAction(e->
           {
               
               FileChooser fc = new FileChooser();
               fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json Files", "*.JSON"));
               File file = fc.showSaveDialog(null);
               
//             for(ePageModel page : Master.getAllPages()){
//            
//                    page.saveToJson();
//           
//                }
               Master.setAsPortfolioData(file.getAbsolutePath());
           
             
                    });
             
             exportEPortfolioButton.setOnAction(e->
             {
                       {
            
               for(ePageModel page : Master.getAllPages()){
            
                    page.saveToJson();
           
                }
               Master.setPortfolioData();
               
                           try {
                               SiteExporter se = new SiteExporter(Master.getAbsPath());
                           } 
                           
                           catch (IOException ex) {
                               Logger.getLogger(EPortfolioMakerViewer.class.getName()).log(Level.SEVERE, null, ex);
                           }
                 
             
             
             }
   }
             );
       }
                     
      public void setupView()
      {
         ePortfolioRootPane = new BorderPane();
         EditorPane = new BorderPane();
         ePortfolioRootPane.setTop(fileToolbarPane);
        ePortfolioRootPane.setCenter(EditorPane);
        EditorPane.setLeft(PageToolBar);
        EditorPane.setTop(TitleBox);
        EditorPane.setCenter(Master);
       
        EditorPane.setBottom(workspaceSwitch);
        


      
      
      }
 
      public void startUI(Stage initPrimaryStage, String windowTitle) {
	
	


	this.primaryStage = initPrimaryStage;

	initWindow(windowTitle);
       
    }
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        initAllToolbars();
        setupView();
        activateEventHandlers();
        activeToolbarHandlers();
       
        
        
        
	primaryScene = new Scene(ePortfolioRootPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        // my background
        
       primaryStage.setScene(primaryScene);
       //activateEventHandlers();

	primaryStage.show();
    }
    public ePageModel getSelectedEPage()
    {        
    
    return (ePageModel) Master.getSelectionModel().getSelectedItem();
    }
        
    public void reloadToolbar()
    {
        
       
 
        if (Master.getAllPages().size() > 0)
        {
         saveEPortfolioButton.setDisable(false);
        saveAsEPortfolioButton.setDisable(false);
        exportEPortfolioButton.setDisable(false);
        
        removeEPageButton.setDisable(false);
    
    // Button for Comp Toolbar
    AddTextComponent.setDisable(false);
    AddVideoComponent.setDisable(false);
    AddSSComponent.setDisable(false);
    AddFooter.setDisable(false);
    AddListComponent.setDisable(false);
    AddHeader.setDisable(false);
    AddImage.setDisable(false);
    AddBanner.setDisable(false);
     RemoveComponent.setDisable(false);
            
            
        }
        else
        {
        saveEPortfolioButton.setDisable(true);
        saveAsEPortfolioButton.setDisable(true);
        exportEPortfolioButton.setDisable(true);
          
        
         removeEPageButton.setDisable(true);
    
    // Button for Comp Toolbar
    AddTextComponent.setDisable(true);
    AddVideoComponent.setDisable(true);
    AddSSComponent.setDisable(true);
    AddFooter.setDisable(true);
    AddListComponent.setDisable(true);
    AddHeader.setDisable(true);
    AddImage.setDisable(true);
    AddBanner.setDisable(true);
     RemoveComponent.setDisable(true);
        
        }
    
    
    }
    
   
    

}
  

