/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolio.View;

import ePage.Components.Component;
import ePage.Components.TextComponent;
import ePage.Components.VideoComponent;
import ePortfolioObj.ePageModel;
import java.io.File;
import java.util.ArrayList;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Yormax
 */
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;

/**
 * This class serves to present a dialog with three options to the user: Yes,
 * No, or Cancel and lets one access which was selected.
 *
 * @author Richard McKenna
 */
public class ActionDialogsBox extends Stage {

    // GUI CONTROLS FOR OUR DIALOGUE
// root of the dialogue box    

    BorderPane root;
    Stage primaryStage;
    Stage VideoStage;

    VBox messagePane;
    VBox ContentBox;
    Scene messageScene;
    Label messageLabel;
    Label NameLabel;
    TextField NameField;
    TextArea TextContent;
    String CompName;
    Button SetNameButton;
    Button SetButton;
    Button cancelButton;
    String selection;

    // CONSTANT CHOICES
    public static final String Set_String = "Set";
    public static final String CANCEL = "Cancel";
    private Object mainStage;
    private Object fileChooser;
    
    
  

    /**
     * Initializes this dialog so that it can be used repeatedly for all kinds
     * of messages.
     *
     * @param primaryStage The owner of this modal dialog.
     */
    public ActionDialogsBox(Stage primaryStage) {
        this.primaryStage = new Stage();
    }

    public void MakeAddEPage(ePageModel newTab) {
        initModality(Modality.WINDOW_MODAL);
        String theName;
        VBox NameBox = new VBox();
        NameLabel = new Label("Please enter the Name of the Webpage :");
        NameField = new TextField();
        NameBox.getChildren().addAll(NameLabel, NameField);
        NameBox.setSpacing(5.0);
        NameBox.setAlignment(Pos.CENTER);

        SetButton = new Button("Set Name");
        SetButton.setPrefWidth(100.0);

        cancelButton = new Button("Cancel");
        cancelButton.setPrefWidth(100.0);

        HBox ButtonBox = new HBox();
        ButtonBox.getChildren().addAll(cancelButton, SetButton);
        ButtonBox.setSpacing(5.0);
        ButtonBox.setAlignment(Pos.BOTTOM_CENTER);

        root = new BorderPane();
        //root.setStyle("-fx-background-color: aqua;");
        root.setCenter(NameBox);
        root.setBottom(ButtonBox);

        SetButton.setOnAction(e -> {

            if (NameField.getText().isEmpty()) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText("Look, an Information Dialog");
                alert.setContentText("Your Page Name is Empty thus it can't be created");

                alert.showAndWait();

            } else {
                newTab.setText(NameField.getText());
                newTab.setName(NameField.getText());
                primaryStage.close();
            }

        }
        );
        cancelButton.setOnAction(e -> {
            primaryStage.close();
        }
        );
        Scene main = new Scene(root, 250, 250);
        primaryStage.setTitle("Text Component");
        primaryStage.setScene(main);
        primaryStage.showAndWait();

    }


}
