/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EPortfolio.View;

import ePage.Components.BannerComponent;
import ePage.Components.Component;
import ePage.Components.FooterComponent;
import ePage.Components.HeaderComponent;
import ePage.Components.ImageComponent;
import ePage.Components.ListComponent;
import ePage.Components.SlideShowComponent;
import ePage.Components.TextComponent;
import ePage.Components.VideoComponent;
import static ePortfolioGenerator.StartupConstants.VIDEO_COMPONENT_IMG;
import ePortfolioObj.ePageModel;
import java.io.File;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author ycastro
 */
public class ActionHbox extends Stage {
    
    // GUI CONTROLS FOR OUR DIALOGUE
// root of the dialogue box    

    BorderPane root;
    Stage primaryStage;
    Stage VideoStage;

    VBox messagePane;
    VBox ContentBox;
    Scene messageScene;
    Label messageLabel;
    Label NameLabel;
    TextField NameField;
    TextArea TextContent;
    String CompName;
    Button SetNameButton;
    Button SetButton;
    Button cancelButton;
    String selection;

    // CONSTANT CHOICES
    public static final String Set_String = "Set";
    public static final String CANCEL = "Cancel";
    private Object mainStage;
    private Object fileChooser;
   public ActionHbox()
   {}
    
    
       public SlideShowComponent MakeSSComponent(ArrayList<Component> listComp, VBox content)
{
        SlideShowComponent newSS = new SlideShowComponent();
        listComp.add(newSS);
        
          newSS.getRemover().setOnAction(e->
        {
            
        listComp.remove(newSS);
        content.getChildren().remove(newSS);
        
        
        }
        );
        
        return newSS;
} 
   

    public VideoComponent MakeVideoComponent(ArrayList<Component> listComp, VBox content)
{
        VideoComponent newVid = new VideoComponent();
        newVid.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
        newVid.getRemover().setOnAction(e->
        {
            
        listComp.remove(newVid);
        content.getChildren().remove(newVid);
        
        
        }
        );
        listComp.add(newVid);
        return newVid;
} 

public TextComponent MakeTextComponent(ArrayList<Component> listComp , VBox content )
{
        TextComponent newTxt = new TextComponent();
        
        newTxt.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
        newTxt.getRemover().setOnAction(e->
        {
            
        listComp.remove(newTxt);
        content.getChildren().remove(newTxt);
        
        
        }
        );
        
        listComp.add(newTxt);
        return newTxt;
} 
    
public ImageComponent MakeImage(ArrayList<Component> listComp , VBox content)
{
        ImageComponent newIMG = new ImageComponent();
          newIMG.getRemoveBox().setAlignment(Pos.TOP_RIGHT);
        newIMG.getRemover().setOnAction(e->
        {
            
        listComp.remove(newIMG);
        content.getChildren().remove(newIMG);
        
        
        }
        );
        
        listComp.add(newIMG);
        return newIMG;
} 
public HeaderComponent MakeHeader(ArrayList<Component> listComp ,VBox content)
{
      HeaderComponent newHeader = new HeaderComponent();
     newHeader.getRemoveBox().setAlignment(Pos.TOP_RIGHT);

      newHeader.getRemover().setOnAction(e->
        {
            
        listComp.remove(newHeader);
        content.getChildren().remove(newHeader);
        
        
        }
        );
      
        listComp.add(newHeader);
        return newHeader;
}
public FooterComponent MakeFooter(ArrayList<Component> listComp, VBox content)
{
      FooterComponent newFooter = new FooterComponent();
        listComp.add(newFooter);
         newFooter.getRemover().setOnAction(e->
        {
            
        listComp.remove(newFooter);
        content.getChildren().remove(newFooter);
        
        
        }
        );
      
      listComp.add(newFooter);
       
        return newFooter;
}
public ListComponent MakeList(ArrayList<Component> listComp, VBox content)
{
      ListComponent newList = new ListComponent();
 newList.getRemover().setOnAction(e->
        {
            
        listComp.remove(newList);
        content.getChildren().remove(newList);
        
        
        }
        );
      
      listComp.add(newList);
        return newList;
} 
public BannerComponent MakeBanner(ArrayList<Component> listComp , VBox content )
{
      BannerComponent newBanner = new BannerComponent();
        listComp.add(newBanner);
        newBanner.getRemover().setOnAction(e->
        {
            
        listComp.remove(newBanner);
        content.getChildren().remove(newBanner);
        
        
        }
        );
        
        
        return newBanner;
}
}

