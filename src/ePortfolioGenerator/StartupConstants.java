/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator;

/**
 *
 * @author yormaxpc
 */
public class StartupConstants {


    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_ePortfolio = PATH_DATA + "ePorfolio/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_VIDEO = "./videos/";
    public static String PATH_JSON = "./Data/";
    
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";


    
    public static String PATH_CSS = "/EPortfolio/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolio.css";


    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_WINDOW_LOGO = "SSM_Logo.png";
    public static String ICON_NEW_SLIDE_SHOW = "New.png";
    public static String ICON_LOAD_SLIDE_SHOW = "LoadOrig.png";
    public static String ICON_SAVE_SLIDE_SHOW = "Save.png";
    public static String ICON_VIEW_SLIDE_SHOW = "View.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_SLIDE = "Add.png";
    public static String ICON_REMOVE_SLIDE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_SAVE_AS = "SaveAs1.png";
    public static String ICON_EXPORT = "Export1.png";
    public static String ICON_DELETE = "DeleteOrig.png";
    public static String ICON_GLOBE = "Globe.png";
    //Here are the Component Images 
    public static String TEXT_COMPONENT_IMG = "file:./images/Comp Images/text.PNG";
    public static String VIDEO_COMPONENT_IMG = "file:./images/Comp Images/video.JPG";
    public static String IMAGE_COMPONENT_IMG = "file:./images/Comp Images/image.JPG";
    public static String SLIDESHOW_COMPONENT_IMG = "file:./images/Comp Images/slideshow.PNG";


    





    // UI SETTINGS
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_EPortfolio_EDIT_VBOX = "ePortfolio_edit_vbox";
    public static String    CSS_CLASS_EPage_EDIT_VIEW = "ePage_edit_view";
    public static String    CSS_CLASS_SELECTED_SLIDE_EDIT_VIEW = "selected_ePage_edit_view";
    //my style Sheets 
    public static String    CSS_CLASS_Main_Background="main_background";
    
    // UI LABELS
    public static String    LABEL_EPortfolio_TITLE = "EPortfolio_title";
    public static String    OK_BUTTON_TEXT = "OK";
}

    

