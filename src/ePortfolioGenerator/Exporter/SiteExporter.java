/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SiteExporter;

import ePortfolioGenerator.EPortfolioGenerator;
import ePortfolioObj.ePageModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author ycastro
 */
public class SiteExporter {
    
    
     String MasterJsonPath = "";
     File MasterJson; 
     ArrayList <String>pageNames = new <String> ArrayList(); 
     ArrayList<String> newPagePath = new <String>ArrayList();
     ArrayList<String> mediaPaths = new <String>ArrayList();

     //Paths
      public static String DATA_DIR = "data/";
      public static String MEDIA_DIR = "media/";
      public static String SLASH = "/";
      public static String SITES_DIR = "./sites/";
      public static String CSS_DIR = "css/";
      public static String JS_DIR = "js/";
      public static String JSON_DIR = "json/";
      public static String IMG_DIR = "imgs/";
      public static String VID_DIR = "vid/";
      public static String BASE_DIR = "./base/";
      public static String HTML_EXT = ".html";
      
     
     public SiteExporter(String MasterJsonPath) throws IOException
     {
         this.MasterJsonPath ="file:"+ MasterJsonPath;
         MasterJson = new File(MasterJsonPath);
         System.out.println(MasterJson.getAbsolutePath());
         readAllPages(MasterJson);
         copyAllJsonFiles();
         generateMedia();
         
         //System.out.print(MasterJsonPath);
         
     
     }
     public void readAllPages(File MasterJson)
     {
     
     try {
                InputStream is = new FileInputStream(MasterJson);
                JsonReader jReader = Json.createReader(is);
                JsonObject ob = jReader.readObject();
                  
               
                     
                 
                 JsonArray pagesArray = ob.getJsonArray("pages");
                 
                 
                        for(int j = 0; j < pagesArray.size(); j++)
                      {
                          
                     
                     JsonObject page = pagesArray.getJsonObject(j);
                    String pageName = page.getString("page name")+".json"; 
                   
                    pageNames.add(pageName);
                           
                       
                           
                      }
               
     
     }
      catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
    
    
    
}
    public void copyAllJsonFiles() throws IOException
    {
        // copy from testjson
        for (String path :pageNames )
        {
            
            String oldPathString  = "./testjson/"+path;
            System.out.println(path);
            String newPathString = "./WebExport/json/"+path;
            
            
            
            
            Path p = new File(oldPathString).toPath();
            Path q = new File(newPathString).toPath();
            newPagePath.add(newPathString);
           
   
        Files.copy(p, q);
           
         
           
        }
    
    
    
    }
    
   
    public void generateMedia() throws IOException
   {
       for (String newPath : newPagePath){
        try {
                InputStream is = new FileInputStream(newPath);
               JsonReader jReader = Json.createReader(is);
                JsonObject ob = jReader.readObject();
                  
               
                     
                
                 JsonArray componentArray = ob.getJsonArray("components");
                 
                 
                          for(int j = 0; j < componentArray.size(); j++)
                     
                          
                      {
                          
                          System.out.println(componentArray.getJsonObject(j).getString("type"));
                          switch (componentArray.getJsonObject(j).getString("type"))
                          {
                              case"image":
                                  
                                  
                                  String oldPathString  = componentArray.getJsonObject(j).getString("filepath");
                                 
                                 System.out.println(oldPathString);

                                  String oldname = new File(oldPathString).getName();
                                String newPathString = "./WebExport/media/"+oldname;
                                System.out.println(newPathString);
            
            
            
            
                            Path p = new File(oldPathString).toPath();
                            Path q = new File(newPathString).toPath();
                            newPagePath.add(newPathString);
           
   
                            Files.copy(p, q);
                            break;
                           case"video":
                                  
                                  
                                  String oldPathString1 = componentArray.getJsonObject(j).getString("filepath");
                                 
                                 System.out.println(oldPathString1);

                                  String oldname1 = new File(oldPathString1).getName();
                                String newPathString1 = "./WebExport/media/"+oldname1;
                                System.out.println(newPathString1);
            
            
            
            
                            Path p1 = new File(oldPathString1).toPath();
                            Path q1 = new File(newPathString1).toPath();
                            newPagePath.add(newPathString1);
           
   
                            Files.copy(p1, q1);
                            break;
                               
                         case"banner":
                                  
                                  if (componentArray.getJsonObject(j).getBoolean("inculdeimage"))
                                  {
                                  String oldPathString2 = componentArray.getJsonObject(j).getString("imagepath");
                                 
                                 System.out.println(oldPathString2);

                                  String oldname2 = new File(oldPathString2).getName();
                                String newPathString2 = "./WebExport/media/"+oldname2;
                                System.out.println(newPathString2);
            
            
            
            
                            Path p2 = new File(oldPathString2).toPath();
                            Path q2 = new File(newPathString2).toPath();
                            newPagePath.add(newPathString2);
           
   
                            Files.copy(p2, q2);
                                  }
                            break;
                          
                          
                          }
                          
                          
                     

                      }
                          
                          
                          }
                          
                          
                     

        catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
                      }
        
        
        }
        
     
     }
      
    
   
    
   
    
     



     