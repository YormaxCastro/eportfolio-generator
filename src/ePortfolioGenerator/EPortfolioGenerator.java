/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolioGenerator;

import EPortfolio.View.EPortfolioMakerViewer;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Yormax
 */
public class EPortfolioGenerator extends Application {

    /**
     * @param args the command line arguments
     */
         public void start(Stage primaryStage) throws Exception {
          EPortfolioMakerViewer ui = new EPortfolioMakerViewer();

        
          ui.startUI(primaryStage, "MyApp");
	
//	// SET THE WINDOW ICON
//	String imagePath = PATH_IMAGES + ICON_WINDOW_LOGO;
//	File file = new File(imagePath);
//	
//	// GET AND SET THE IMAGE
//	URL fileURL = file.toURI().toURL();
//	Image windowIcon = new Image(fileURL.toExternalForm());
//	primaryStage.getIcons().add(windowIcon);
         //>.getStylesheets().add(StartupConstants.PATH_CSS+"SlideShowMakerStyle/main_Background.css");

	
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
       

	    // NOW START THE UI IN EVENT HANDLING MODE
	 // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	
    }
     public static void main(String[] args)
      {
       launch(args);
       
       String jsonFilePath = "testjson/test.json";
       JsonObject jso0 = Json.createObjectBuilder()
               .add("width", "100")
               .add("height", "200")
               .add("caption", "the caption")
               .build();
       
         JsonObject jso1 = Json.createObjectBuilder()
               .add("width", "100")
               .add("height", "200")
               .add("caption", "the caption")
               .build();
         
         JsonArrayBuilder ja = Json.createArrayBuilder()
                 .add(jso0)
                 .add(jso1);
         
         JsonArray array = ja.build();
         
             try {
                 
                Map<String, Object> properties = new HashMap<>(1);
                 properties.put(JsonGenerator.PRETTY_PRINTING, true);
                 
                OutputStream os = new FileOutputStream(jsonFilePath);
                 
                JsonWriterFactory jf = Json.createWriterFactory(properties);
                JsonWriter jWriter = jf.createWriter(os);
                 
                 jWriter.writeArray(array);
                 jWriter.close();
                 os.close();
                 
             } 
             catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
             // Reader
             try {
                 InputStream is = new FileInputStream(jsonFilePath);
                 JsonReader jReader = Json.createReader(is);
                 
                 JsonArray jArray = jReader.readArray();
                 
                 for(int i = 0; i < jArray.size(); i++){
                     JsonObject ob = jArray.getJsonObject(i);
                     System.out.printf("width: %s height %s caption %s \n", ob.getString("width"), ob.getString("height"), ob.getString("caption"));
                 }
                 
                 
             }
             catch (IOException ex) {
                 Logger.getLogger(EPortfolioGenerator.class.getName()).log(Level.SEVERE, null, ex);
             }
       
       
       

          
          
      }
    
    
}
