/* Constants needed for accessing JSON key's */
var HEADER;
var NAME;
var LINKS_LIST;
var LIST_COMPONENT
var LIST_TITLE;
var LIST_INFO;
var VIDEO_COMPONENT;
var IMG_WIDTH;
var SCALED_IMAGE_HEIGHT;

/* Slideshow attributes */
var FADE_TIME;
var SLIDESHOW_SLEEP_TIME;
/* Slideshow */
var slides;
var currentSlide;
var title;



function initWebPage()
{
    var webPageData="./json/samp_json.json";
    initConstants();
    loadWebPage(webPageData);
}

/* Slide obj */
function Slide(initImgFile, initCaption)
{
    this.imgFile = initImgFile;
    this.caption = initCaption;
}

function initConstants()
{
    HEADER = 'header';
    NAME = 'name';
    LINKS_LIST = 'links_list';
    LIST_COMPONENT = 'list_component'
    LIST_TITLE = 'list_title';
    INFO_LIST = 'info_list';
    VIDEO_COMPONENT='video_component'
    IMG_WIDTH = 1000;
    SCALED_IMAGE_HEIGHT=300.0;
    
    /* slide show */
    FADE_TIME = 1000;
    SLIDESHOW_SLEEP_TIME = 3000;
    slides = new Array();
     
}

function loadWebPage(webPageData)
{
    $.getJSON(webPageData, function(json_webpage) {
        loadFont(json_webpage);
        loadHeader(json_webpage);
        loadListComponent(json_webpage);
        loadTextComponent(json_webpage);
        resizeImageTextComp();
        loadImageComponent(json_webpage);
        resizeImageComp();
        loadVideoComponent(json_webpage);
        loadSlideShowComponent(json_webpage);
    });
}
function loadSlideShowComponent(json_webpage)
{
    
    
}
function loadFont(json_webpage)
{
    var includesFont = json_webpage.hasOwnProperty('font');
    if(includesFont)
    {
        $("#font_link").attr('href',json_webpage.font.link);
        $("#conainer ").css("font-family", json_webpage.font.fontfamily);
    }
}

function loadHeader(json_webpage)
{
    var includesHeader = json_webpage.hasOwnProperty(HEADER);
    var header         = json_webpage[HEADER];
    var includesName   = header.hasOwnProperty(NAME);
    var includesLinks  = header.hasOwnProperty(LINKS_LIST);
    var links          = json_webpage.header.links_list;
    var links_size     = links.length;
    
    /* Render Screen based on information provided by json */
    if(includesHeader) {
        if(includesName)
            $("#header_title").html("<a class=navbar-brand href= #" + ">" + json_webpage.header.name + "</a>");
        
        if(includesLinks) {
            /* Search through provided links */
            for(var i = 0; i < links.length; i++) 
                $("#navbar_links").append("<li><a href=" + links[i].link + ">" + links[i].title + "</a></li>");            
        }
    }
}

function loadTextComponent(json_webpage)
{
    for(var i = 0; i < json_webpage.text_component.length; i++) {
        $("#text_component").append("<div><h1>" + json_webpage.text_component[i].header + "</h1><hr>" +"<img src=" + json_webpage.text_component[i].img + "><p>" + json_webpage.text_component[i].summary + "</p></div>");
    }
}



/* loading lists */
function loadListComponent(json_webpage)
{
    var includesListComponent = json_webpage.hasOwnProperty(LIST_COMPONENT);
    var list_component = json_webpage.list_component;
    var list_component_size = list_component.length;
    
    if(includesListComponent)
    {
        $("#list_component").append("<h1>Heres a list</h1>");
        for(var i = 0; i<list_component_size; i++)
        {
            var caption = json_webpage.list_component[i].list_title;
            $("#list_component").append("<div id =" + i + " class='col-md-4' style='background-color: lightcyan'></div>");
            $("#" + i).append("<h2>" + caption+"</h2>");
            for(var j = 0; j < json_webpage.list_component[i].info_list.length; j++) {
                $("#" + i).append("<li>" + json_webpage.list_component[i].info_list[j].item +"</li>");
            }
        }
    }
}

/* load slideshows */
function loadSlideShow(json_webpage)
{
    var includeSlidShowComponent = json_webpage.hasOwnProperty(SLIDE_CONTENT);

    if(includeSlideShowComponent) {
    }
}

/* loading images from json */
function loadImageComponent(json_webpage)
{
    var includeImageComponent  = json_webpage.hasOwnProperty('image_component');
    var imageComponentListSize = json_webpage.image_component.length;
    if(includeImageComponent) {
        $("#image_component").append("<h1>Random Pictures</h1>");
        for(var i = 0; i < imageComponentListSize; i++)
        {
            $("#image_component").append("<img src=" + json_webpage.image_component[i].src + "/>");
        }
    }
}

/* load videos from json */
function loadVideoComponent(json_webpage)
{
    var includeVideoComponent = json_webpage.hasOwnProperty(VIDEO_COMPONENT);
    var videoListCount = json_webpage.video_component.length;
    
    if(includeVideoComponent)
    {
       $("#video_component").append("<h1>Heres a Video</h1>");
        for(var i = 0; i < videoListCount; i++) {
            $("#video_component").append("<h2>" + json_webpage.video_component[i].caption + "</h2>");
            $("#video_component").append("<iframe src=" + json_webpage.video_component[i].video_src + ">" + "</iframe>");           
        }
    }
}

/* resizing images */

function resizeImageTextComp() {
    $('#text_component img').each(function() {
        $(this).load(function() {
            var origHeight = $(this).height();             
            var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
            var origWidth = $(this).width();             
            var scaledWidth = origWidth * scaleFactor;
            $(this).height(SCALED_IMAGE_HEIGHT);
            $(this).width(scaledWidth);
        })
    });
}

function resizeImageComp() {
    $('#image_component img').each(function() {
        $(this).load(function() {
            var origHeight = $(this).height();             
            var scaleFactor = SCALED_IMAGE_HEIGHT/origHeight;
            var origWidth = $(this).width();             
            var scaledWidth = origWidth * scaleFactor;
            $(this).height(SCALED_IMAGE_HEIGHT);
            $(this).width(scaledWidth);
        })
    });
}


